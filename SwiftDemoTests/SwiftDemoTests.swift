//
//  SwiftDemoTests.swift
//  SwiftDemoTests
//
//  Created by Hari Giri (IOS) on 12/08/21.
//

import XCTest
@testable import SwiftDemo

class SwiftDemoTests: XCTestCase {
    
    let serviceHandle = ServiceHandle()
    
//    func testAddition(){
//        let output = serviceHandle.additionOfTwoNo(numberOne: 2, numberTwo: 3)
//        print(output)
//    }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws{
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
