//
//  NotificationService.swift
//  ServiceExtention
//
//  Created by Hari Giri (IOS) on 14/12/21.
//

//
//  NotificationService.swift
//  NotificationServiceExtension
//
//  Created by Hari Giri (IOS) on 13/12/21.
//

import UserNotifications


extension UNNotificationRequest {
    var attachment: UNNotificationAttachment? {
        guard let attachmentURL = content.userInfo["image_url"] as? String, let imageData = try? Data(contentsOf: URL(string: attachmentURL)!) else {
            return nil
        }
        return try? UNNotificationAttachment(data: imageData, options: nil)
    }
}

extension UNNotificationAttachment {

    convenience init(data: Data, options: [NSObject: AnyObject]?) throws {
        let fileManager = FileManager.default
        let temporaryFolderName = ProcessInfo.processInfo.globallyUniqueString
        let temporaryFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(temporaryFolderName, isDirectory: true)

        try fileManager.createDirectory(at: temporaryFolderURL, withIntermediateDirectories: true, attributes: nil)
        let imageFileIdentifier = UUID().uuidString + ".jpg"
        let fileURL = temporaryFolderURL.appendingPathComponent(imageFileIdentifier)
        try data.write(to: fileURL)
        try self.init(identifier: imageFileIdentifier, url: fileURL, options: options)
    }
}


class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
              
        
                defer {
                    contentHandler(bestAttemptContent ?? request.content)
                }

                guard let attachment = request.attachment else { return }

                bestAttemptContent?.attachments = [attachment]
        
                if let bestAttemptContent = bestAttemptContent {
                    // Modify the notification content here...
                    bestAttemptContent.title = "Hari GIRI\(bestAttemptContent.title) [modified]"
                    contentHandler(bestAttemptContent)
                }
        
    }
    
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}


/*
 
 {
   "aps": {
     "alert": {
       "title": "New Podcast Available",
       "subtitle": "Antonio Leiva – Clean Architecture",
       "body": "This episode we talk about Clean Architecture with Antonio Leiva."
     },
     "mutable-content": "1"
   },
   "podcast-image": "https://koenig-media.raywenderlich.com/uploads/2016/11/Logo-250x250.png",
   "podcast-guest": "Antonio Leiva"
 }
 */
