//
//  ComputedAndStorePropertiesCodable.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 31/12/21.
//
//https://docs.swift.org/swift-book/LanguageGuide/Properties.html
/*
 Lazy Stored Properties
 * Lazy property is that whose initial value is not calculate until its first time used. It is always a var because its initial value is nil it may be change in future when it is
 use.
*/

/*
 Computed Propeties
 * Computed properties calculate the value rather than store.
 * Computed properties are provided by  class, struct and enumuration.
 * Computed properties has getter and option setter to retrieve and set other properties and value indirectly.
 */

/*
 Store Properties
 * Store properties store the constant(let) and variable(var) value as a part of instance.
 * Store propertis are provide by only class and struct
 */

/*
 Property Observers
 * We can define property observers to monitor change in property value.
 * There are two types of observers willSet and didSet.
 * willSet: It is called just before the value is store.
 * didSet: It is called immedietly after the value is store.
 
 You can add property observers in the following places:
     Stored properties that you define
     Stored properties that you inherit
     Computed properties that you inherit
 */


/*
 https://www.youtube.com/watch?v=6kq1F8ST5Vc
 Property wrapper
 */

/*
 Secure API
 Almost all the iOS apps have some secret keys to protect like:
 API keys
 Private Headers
 Cryptographic Keys
 Passwords
 
 //"Content-Type": "application/json/text/ascii/utf(NSData)", request body type
 //Authorization: ""Secure key

 //https://medium.com/swift-india/secure-secrets-in-ios-app-9f66085800b4
 * Using authorization header like Bearer with key(which provided by server), Using API Key(Key, Value)
 * API Key(For Secure)(key,value,header): We need to create .Config file and added to need api keys , api method and url. Added(write xyz.config) the .config file in gitignore file for this other person will not see the .config file physically when they access the same repository.
 * Using JWT(JSON Web Token, sh256) token. We pass the JWT token to header(Combination of Payload+header+signature(sha256))
 * Basic Auth(Username, Password)
 */

/*
 //https://www.youtube.com/watch?v=kNHZDzQOii4
 //https://medium.com/@anuj.rai2489/ssl-pinning-254fa8ca2109
 import Security
 import CommonCrypto
 
 SSL Paining -Secure Socket Layer
 1. Certificate Pinning
 2. Public key Painning - (We can get key from terminal or via code)the underlying public key within the certificate remains the same. 
 
 step1: First client request the connection request to server.
 step2: Server give response included public key and certificate.
 step3: Client check the certificate and send the encrypted key to server.
 step4:  Server decrypt the key and send the encrypted data to client.
 step5: Client recieve the encrypted data and decrypt the same.
 
 
 *
 */


/*
 Codable:  public typealias Codable = Decodable & Encodable
 *Introduced in Swift 4
 *Codable is to convert the JSON data object to an actual Swift class or struct and vice versa.
 * Codable is actually a type alias that combines two protocols — Encodable and Decodable
 * Encode and Decode data serialize format just like json.(When we pass the data to server at that time encode the data and when we get the data from server at that time decode the that data)
 
 *In Swift & is the same as , in protocol composition so Encodable, Decodable = Encodable & Decodable = Codable
 
 CodingKeys(Protocol): It is used when we want to change the key name
 */
/*
 JSONSerialization
 */



/*
 NSURLConnection:
 * Its not support background download
 *  can not cancel the request
 * delegation based
 URLSession:
 *Its support background download when app is in background and not running mode
 *can cancel the request(when we request the group of api we can cancel any one of them)
 *block based
 *NSURLSessionDataTask,NSURLSessionUploadTask,NSURLSessionDownloadTask
 */


/*
 ABI/Application Binary Interface
 * At run time(after downloaded from appstore),Swift program binary(ipa or app) interact with other libraries and module through an ABI. ABI is working with OS to full fill the requirment of Swift program binary.
 * Due to ABI bundle size is reduced(because it is not containt the dynamic library in framwork folder in ipa file before swift 5)
 * Newer compiler can compile the written code in an older version of swift.
 * App build in other version and swift compiler interact with other version of library.
 * Easy to convert one version of swift code to other version
 * Its not custom module stable (we have to handle the module compatibilty at compile time)
 * Every version of app will  not have thier own ABI embedded with thier binary its has only one ABI which is embedded with iphone OS. that abi is compatible for like 5.0 and 5.2 verion

 
 ** Before Swift 5, Swift was not ABI Stable. Swift dynamic library embedded into app bundle to support spacific version of swift which in app build due to this app size is big and as well as other swift version not support with that spacific created build.
 ** Every version of app will have thier own ABI embedded with thier binary.
 ** The ABI is working with in spacific for every build or ipa or app.
 ** Its default to convert code one version of swift code to other version
 
 */
//ipa= iPhone application archive
//    open func encode<T>(_ value: T) throws -> Data where T : Encodable
//* why we use where


/*
 func encode(with aCoder: NSCoder) {
   //add code here
 }
 required convenience init?(coder aDecoder: NSCoder) {
   //add code here
   self.init(title: "", rating: 0)
 }
*/


/*
 ***Instead of generic types we can specify Any/AnyObject, but that will affect performance since the type of object will be determined at runtime.
 
 Any: Its working with any types of swift. Support hetrogenius
 AnyObject: Its only work with referance type its good when we dealing with objective c at that time AnyObject can use because ojective c work with referance type. Support homogenius
 * AnyObject is also used when you want to restrict a protocol so that it can be used only with classes.
  Where: It is just like condition it define that some value type is this. we can use this like in Array(generic which accept all type but we can fix only for string using where keyboard)
 AnyHashable:
 NSObject:The root class of most Objective-C class hierarchies, from which subclasses inherit a basic interface to the runtime system and the ability to behave as Objective-C objects.
 NSCoding:
*/
//[weak self] = self having the strong ref type by doing weak self the self is now weak for this reason closure  capture weak self other wise clouse having strong referance of self due to this it will issue in memory


/*
Open — This is where you can access all data members and member functions within the same module(target) and outside of it. You can subclass or override outside the module(target).
Public — This is the same as open, the only difference is you can’t subclass or override outside the module(target).
Internal — This is the default access level in Swift, it allows all data members and member functions to be accessed within the same module(target) only and restrict access outside the module(target).
Private — This is where you can access data members and function within its enclosing declaration as well as an extension within the same file. It does not allow access in a subclass with in the same file or in another file.
File-private - This is the same as private, the only difference is it allows access in a subclass with in the same file.
*/


/*
 Internal - This is default access specifier in swift. With this we can access data members and member functions in the same module (target).
 Public - This is where you can access all data members and member functions within same module and outside of it. But you can't subclass or override outside the module.
 Open - same as public, only difference is you can subclass or override outside the module.
 Fileprivate - As the name say's, data members and member functions are accessible within the same file.
 Private - This is where you can have access within the scope of function body or class.
 */


import UIKit
import CoreGraphics


class ComputedAndStorePropertiesCodable: UIViewController {
    
    //Lazy
   lazy var name: String = "Holostik"
   lazy var lazyName: String = { // lazy store property
        return "Lazy\(name)"
    }()
    
    @IBOutlet weak var productImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print(name)
        print(lazyName)
        print(computedValue)
        
        print(hightScore = 10)
        
        getAPIKEY()
        codable()
        logicalF1()
        
        var cc: [Int] = [Int](repeating: 0, count: 5)
        var array = [Int]()
        array.reserveCapacity(10)
        
        self.view.clipsToBounds = false
        
        let button = UIButton()
        button.frame = CGRect(x: 100, y: 100, width: 100, height: 100)
        button.backgroundColor = .red
        button.addTarget(nil, action: #selector(AppDelegate.buttonActions), for: .touchUpInside)
        self.view.addSubview(button)
        
        productImage.image = resizedImage(size: CGSize(width: self.view.frame.size.width - 40, height: 300))
        
    }


    // Technique #1 //UIKIT
    func resizedImage(size: CGSize) -> UIImage? {
        
        //https://nshipster.com/image-resizing/
         let image = #imageLiteral(resourceName: "soso")

        let renderer = UIGraphicsImageRenderer(size: size)
        return renderer.image { (context) in
            image.draw(in: CGRect(origin: .zero, size: size))
        }
    }
    
    @objc func buttonActions(sender: UIButton){
        print(sender)
//        print(sender.next?.touchesBegan(.init(), with: .none) as Any)
    }
    
    //versa
    //Store
    
    var storeValueA: Int = 2
    var storeValueB: Int = 5
    
    // Computed
    var computedValue: Int{

        get{
            return storeValueA * storeValueB
        }
        set{//shorthand
            print(newValue)
            storeValueA = newValue
        }
//        set(computedNewValue){
//          print(computedNewValue)
//            storeValueA = computedNewValue
//        }
    }
    
    
    //Property Observers
    var hightScore: Int = 0{
        willSet{
            print("new score \(newValue)")
        }
        didSet{
            
            if hightScore > oldValue{
                print("new score added \(hightScore)")
            }else{
                print("old and new score \(oldValue) : \(hightScore)")
            }
        }
    }
    
    //Secure API Key
    func getAPIKEY(){
        
        let api_Key = Bundle.main.infoDictionary?["API_KEY_DIS"]
        print(api_Key as Any)
    }
    
    
    
    @IBAction func computedAction(_ sender: UIButton) {
        computedValue = 20
        print(computedValue)
        print(hightScore = 9)
        propertyWrap()
        self.backGroundTest()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(computedValue)
        deferExm()
        
//        let queue = DispatchQueue(label: "customQueue", qos: .background, attributes: .concurrent)
//        DispatchQueue.global().async {
//        queue.async {
//            self.backGroundTest()
//        }
//        }
    }
    
    
    func backGroundTest(){
        
        let mutableRequest = NSMutableURLRequest(url: NSURL() as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 20.0)
        
        do{
        let jsonData = try JSONSerialization.data(withJSONObject: [:], options: .fragmentsAllowed)
            mutableRequest.httpBody = jsonData
            mutableRequest.allHTTPHeaderFields = ["Content-Type": "Json", "Authorization": "Bearar serverkey"]
            mutableRequest.httpMethod = "POST"
        }catch{
            print("catch")
        }
        
       
        Thread.sleep(forTimeInterval: 10.0)
        
        
        DispatchQueue.global().async {
            URLSession.shared.downloadTask(with: mutableRequest as URLRequest) { (url, response, error) in
                print(error)
            }.resume()
        }
        
   
        DispatchQueue.global().async {
            URLSession.shared.dataTask(with: mutableRequest as URLRequest) { (data, response, error) in
                print(error)
            }.resume()
        }
        
        
        for value in 0...5{
            
//            Thread.sleep(forTimeInterval: 1.0)
            print(value)
            print(Thread.current)
        }
    }
    
    func deferExm(){
        
        /*
        print("start")
        defer{
            print("fisrt block")
        }
        defer{
            print("second block")
        }
        print("end block")
 */
        /*
        print("Step 1")

        do {
            defer { print("Step 2") }
            print("Step 3")
            print("Step 4")
        }

        print("Step 5")
        */
        
        
        for i in 1...5 {
            print ("In \(i)")
            defer { print ("Deferred \(i)") }
            print ("Out \(i)")
        }
}
    
    
  
    
    // Codable
    func codable(){
        
        do {
            let userCodable = UserCodable(userId: "hari@gmail.com", password: "123")
            let encodeCodable = JSONEncoder()
            let encodeData = try encodeCodable.encode(userCodable)
            
            let decoder = JSONDecoder()
            let secondUser = try decoder.decode(
                UserCodable.self, from: encodeData)
            print(secondUser)
        }catch{
        print("catch")
        }
    }
    
 
    
//
    //Property Wrapper
    func propertyWrap(){
        var rectangle = SmallRectangle()
        print(rectangle.height)
        // Prints "0"

        rectangle.height = 10
        print(rectangle.height)
        // Prints "10"

        rectangle.height = 24
        print(rectangle.height)
        // Prints "12"
    }
    
    
    
    func logicalF1(){
        
        let battery = UIDevice.BatteryState.full
        print(battery)
        
        switch UIDevice.current.batteryState {
        case .full:
            print("full")
        case .charging:
            print("charging")
        default:
            print("default")
        }
    
        
        let ary: [Any] = [["name": "Ram", "company": "Netflix"],["name": "Bob", "company": "Netflix"],["name": "Shyam", "company": "Amazon"],["name": "Rob", "company": "Google"], ["name": "Nob", "company": "Google"]]
        
        var duplicateIndex = 0
        var name = ""
        var copyAry: [String] = []
        
        
        for dict in  ary{
            
            let dictData = dict as! [String : AnyObject]
            let companyName = dictData["company"] as! String
            
            name = ""
            duplicateIndex = 0
            
            if copyAry.contains(companyName){
                continue
            }
            
            for dict1 in ary{
                
                let dictData1 = dict1 as! [ String : AnyObject]
                let companyName1 = dictData1["company"] as! String
                                
                if companyName == companyName1{
                    duplicateIndex += 1
                }
                
                if duplicateIndex == 2 {
                     name = dictData1["name"] as! String
                    copyAry.append(companyName)
                    break
                }
            }
            
                if duplicateIndex == 2{
                    print(companyName + "," + (dictData["name"] as! String) + "," + name)
                }else{
                    print(companyName + "," + (dictData["name"] as! String))
                }
        }
        
        neastToZero()
        
    }
    

    func neastToZero(){
        
        
        let ary = [7,2,1,4,5,-1]
        
        var minValue = 100
        
        for i in 0..<ary.count{
            
            if minValue > ary[i] && i>0 {
                minValue = ary[i]
            }
        }
        print(minValue)
        
        tripletSum()

    }

    
    func tripletSum(){
        
        var array = [2,1,3,4,5,6,7,-1]
        var leftIndex = 0
        var rightIndex = 0
        let sum = 6
        array.sort()
        for i in 0..<array.count{
            
            leftIndex = i + 1
            rightIndex = array.count - 1
            
            while (leftIndex<rightIndex) {
                
                if (array[i] + array[leftIndex] + array[rightIndex] == sum){
                    print(array[i], array[leftIndex], array[rightIndex])
//                    break
                    leftIndex += 1
                }else if (array[i] + array[leftIndex] + array[rightIndex] < sum) {
                    leftIndex += 1
                }else{ //(array[i] + array[leftIndex] + array[rightIndex] > sum)
                    rightIndex -= 1
                }
            }
        }
         multipleOFValue()
    }
    
    
    
    func multipleOFValue(){
        
        do {
        //Copy on Assignment
        let emptyStruct = EmptyStruct() //address A
        let copy = emptyStruct //address B

        //Copy on Write
        let array = [1,2,3] //address C
        var notACopy = array //still address C
        notACopy = [4,5,6] //now address D
        }
        
        
        
        let array = [1,0,8,2,4,6,5,3,10,20]
        
        let sum = 20
        
        for i in 0..<array.count{
            
            for j in i+1..<array.count{
                
//                print("index\(j), value \(array[j])")

                if array[i] * array[j] == sum{
                    print("value (\( array[i])*\(array[j]))= \(array[i] * array[j])")
                }
            }
            
        }
     }
}


//[]

struct SmallRectangle{
    @TwelveOrLess var height: Int
    @TwelveOrLess var width: Int
}



@propertyWrapper
struct TwelveOrLess {
    
    private var number = 0
    var wrappedValue: Int {
        get {return number}
        set {number = min(newValue, 12) }
    }
    
}


//Codable
struct UserCodable: Codable {
    var userId: String
    var password: String
}


struct User: Encodable {
    var name: String
    var age: Int
}





//
//

//Latest Version
/*
Swift: 5.5.2 / 13 Dec 2021
iOS/iPadOS/tvOS: 15.2 /13 Dec 2021
watchOS: 8.3/13 Dec 2021
Xcode: 13.2.1
MacOS: 12.1//Monterey(12)/BigSur(11)/Catalina(10.15)/Mojave(10.14)/High Sierra(10.13)/Sierra(10.12)/El Capitan(10.11)
 
 */
/*
 */

//3 Insaan
//3 rakhas

//IIIRRR
//
//I



/*
 
 import Foundation

 let n = readLine()
 let array = readLine()!.split{ $0 == " " }.map{ Int(String($0)) }
 var minValue = 1000
 for i in 0..<array.count{

      if minValue > array[i]! && array[i]! >= 0{
          minValue = arra//÷≥y[i]!
      }
 }

 print(minValue)

 */


struct EmptyStruct {
}

// ResponderChain===
//https://blog.dunzo.com/swift-responder-chain-19a19fa0fadc
extension UIView{
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("View:touchesBegan")
        next?.touchesBegan(touches, with: event)
    }
    
//        @objc func buttonActions(sender: UIButton){
//            print(sender)
////            next?.touchesBegan(.init(), with: .init())
//        }
}


extension ComputedAndStorePropertiesCodable{
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Controller:touchesBegan")
        next?.touchesBegan(touches, with: event)
        print("next?.touchesBegan(touches, with: event)")
    }
}

extension UIWindow{
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Window:touchesBegan")
        next?.touchesBegan(touches, with: event)
    }
}

extension AppDelegate{
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("AppDelegate:touchesBegan")
        next?.touchesBegan(touches, with: event)
    }
}




