//
//  LibraryFrameworkInitializer.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 15/12/21.

/*
 
Apple SDKs: SDK is collection of framework and library in iOS.
 
 Libraries and frameworks are basic building blocks for creating iOS and macOS programs.

 
 //https://pratheeshbennet.medium.com/static-library-vs-dynamic-library-in-ios-55478ed53a03
 //https://developer.apple.com/library/archive/documentation/DeveloperTools/Conceptual/DynamicLibraries/100-Articles/OverviewOfDynamicLibraries.html
 //https://www.vadimbulavin.com/static-dynamic-frameworks-and-libraries/
 
 
 
Library: Library are collection of code and data or collection of object file and that has only executable code(not contain like image or stroyboard).
 * Library all object and data take memory when we even use single object from library. It will load whole library when it is first time used.
 
 *How to use Static and Dynamic library: Go to Framework Target> Build Setting> Linking and Search Macho-O Type and change static and dynamic library
 
Static Library/.a: When we use static library it will loaded the static lirbary at compile time or launch time and due to this the memory will increase and app lunch time will increase.
 *Static Library all object file and data loaded on memory even when we use single function.
 * Its loaded in excutable file thats why it is faster then dynamic library.
 *Its Guaranteed that static library has currect version of library.
 * When any improvment on static library we have to need to create new build for this.

 
 
Dynamic Library/.dylib: When we use dynamic library it will only loaded on memory when it is used due to this it will not delay the app launch and not increase the memory at launch time.
 *Dynamic Library only loaded  those object file in memory  which is in used.
 * Its not loaded in executable file thats why its slower then static library.
 * Its has a benifit that when any improvment on dynamic library like in system libray(UIkit or Fundation- framework contain dynamic library) we do not need to create new build for this.
 *The Apple recommendation is using dynamic libraries instead of static libraries(UKit,Foundation- framework contain dynamic library).System iOS and macOS libraries are dynamic.
 
 
 *tbd: text based dylib
 
 
Framework: Framework are hierarycal or stracture directries that encapsulate the library, framework, header file, resources such as stroyboard,image and string file into a single package.
 *When we use image resouces in framework we have to add images in .bundle folder.
 *When use use framework then we need to set different path for semulator and actual device.
 
 
Static Framework: It consist of static library
Dyanamic Framewrok: (UIKit- contain dyanamic library, Foundation- contain dyanamic library)It consist of dynamic library.
 
 
*/


/*
 //https://medium.com/flawless-app-stories/static-vs-dynamic-dispatch-in-swift-a-decisive-choice-cece1e872d
 //https://betterprogramming.pub/static-dispatch-over-dynamic-dispatch-a-performance-analysis-47f9fee3803a
 //https://medium.com/@abhishek1nacc/dynamic-static-dispatch-swift-e6e87a462a2a
 //https://betterprogramming.pub/a-deep-dive-into-method-dispatches-in-swift-65a8e408a7d0
 //https://www.rightpoint.com/rplabs/switch-method-dispatch-table
 
 
 Dipatch means sending(somthing and someware).
 Method dispatch is a mechanism to decide which  operation should execute or which method implemantation should be used.
 
 Static/Direct Dispatch: implementation is decide at compile time. Static dispatch support with value type like struct and enum and it also work with ref type like class when we make class final. Its is fast because it decide impleantation at compile time, can not support inheritance
 * Support by ref(final,static) and value type:
 * Only one implementation of method store in memory so compiler will directly execute it.
 * If method is declare and define direct in extension(class,struct,protocol)

  
 
 Dynamic Dispatch: Implementation is decide at runtime. There are two types of dynamic dispatch
 1. Table/witness Dispatch: faster then message,If we have class without final keyword which class have method and this class support inheritance that means that method is Table Dispath.
 * Table Dispatch uses array(dispatch table) of function pointer(addess) to each method in class. if any method added in class it means that it will add in array of end.
 * Every class method imlementation store in array called as dispatch table.
 * Every subclass/class has its own array of function pointer (if we use inheritance if we use same method use in base class it means same method address avaible in different array of function. And compiler will check at run time to optimiz code but not able to it  because same method available(with diff defination) in both array of address that why it slow the static dispatch)
 * Default dispatch method for ref type in swift
 * Dispatch created at compile time.
 * Normal method and @objc method address store in v-table(array)
 
 e.g This explains the reason why table dispatch is slower than static dispatch, because it should traverse jumping through witness tables of superclass, subclasses.
 
 
 
 
 2. Message Dispatch: slower then table, same as Table Dispatch but when method have @objc dynamic keyword it means that method is Message Dispatch. Cocoa library like KVO and Core Data use Message Dispatch. even using this it enable the method swizzling.
 * Its is used when we manupulated thing at runtime.
 * Support by only ref type because dynamic dispatch support inheritance so value type can not support inheritance.
 *This is the default dispatch method in Objective-C
 *The @dynamic keyword is used in Swift to enable dynamic dispatch
 *@objc protocol
*/

/*
 
 @objc =  @objc means you want your Swift code (class, method, property, etc.) to be visible from Objective-C.
 dynamic = dynamic means you want to use Objective-C dynamic dispatch.

 */



/*
*Method swizzling: Change the functionality of method at runtime
 * Objective-C is a very runtime-flexible language and that you can essentially change method implementations, add method implementations, etc., all at runtime.
 *Objective-C is a very runtime-dependent language.
*/

import UIKit
class LibraryFrameworkInitializer: UIViewController {
    override func viewDidLoad(){
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //1
        let defaultClass = DefaultClass()
        let defaultStruct = DefaultStruct()

        //2
        let requiredParentClass = RequiredParentClass()
        requiredParentClass.f1()
        
        //3
        var mutatingStruct = MutatingStruct(name: "Hari")
        mutatingStruct.f1(name: " Giri")
        mutatingStruct.f2(lastName: " holo")
        print(mutatingStruct.name)
        
        print(MutatingStruct.staticValue)
        MutatingStruct.staticValue += 10
        print(MutatingStruct.staticValue)
        
        print(MutatingStruct.staticName)
        MutatingStruct.staticName.append(" giri")
        print(MutatingStruct.staticName)
        
        //4
        let freshersEmp = DesignatedConvenienceClass(freshersEmpName: "Web Developer")//convenience initilizer
        print(freshersEmp.empName)
        let experienceEmp = DesignatedConvenienceClass(experienceEmpEmpName: "Mobile Developer", empSalary: 50000.0)// designated initilizer
        print(experienceEmp.empName)
        
        //5
        let failableClass = FailableClass(totalItems: 4)
        mutatingStruct.returnValueF() //@discardableResult using this it disappear the warning
    
    }
}


//https://docs.swift.org/swift-book/LanguageGuide/Initialization.html
//mutating,convenient,required,override,init,deinit

// initilizer are called to create the new instance.
/*
 1.Default
 *'Default' initilizer does not need init() Method(its already has init method by default) 'default' initilizer only have optional,unwrap and initilized properties.
 */
class DefaultClass{
    var productName: String?
    var quantity: Int = 1
    var isProductAvailable: Bool = true
    var productDesc: String!
  //var address: String // implecitly unwrap property will not accept in default initilizer
}

struct DefaultStruct{
    var productName: String?
    var quantity: Int = 1
    var isProductAvailable: Bool = true
    var productDesc: String!
  //var address: String // implecitly unwrap property will not accept in default initilizer
}


/*
 2.Required
 *When we use 'required' modifier with initilizer  it means  when we inherited this class to other class its mandatory to implement initilizer with 'required' modifier in subclass without overrride modifier.
 */

class RequiredParentClass{
    
    var index = 4
   required init(){
        print("RequiredParentClass")
    }
    func f1(){
        print("Parent f1")
    }
    
    static func staticFunction(){
        
    }
    
}


class RequiredChildClass: RequiredParentClass {

    required init(){//mandatory to implement this
        print("RequiredChildClass")
//            index = 5 // we will not able to access this variable(super class variable) without calling  super.init() method()
    }
    
    override func f1(){
        print("Child f1")
        super.f1()
    }
    
    
}

/*
 3.Mutating
 *Mutating is used when we want to achieve referance type in struct because struct is value type .Mutating modifier is used with normal function can not use with init() method.
 */

struct MutatingStruct{
    
    /*
    var stringg: String // it will not through error if you will not define init() method becase struct has default initilizer if you define init() method it will through error
    
//    init() {
//
//    }*/
    
    var name: String
    static var staticValue: Int = 10
    static var staticName: String = "Hari"

    mutating func f1(name: String){
        self.name.append(name)
    }
    
    func f2(lastName: String)  {
        print(lastName)
//        name = lastName //we can not modify any thing in normal function.
    }
    
    @discardableResult // using this it disappear the warning
    func returnValueF()-> Bool{
        return true
    }
}

/*
 4.Designated and Convenience
 * Designated is a simple initilizer. it does not have any extra modifier with init() method. we can override Designated method in subclass
 * Conveniance initilizer call the designation initilizer from same class but can not override convenience initilizer in subclass
 * Can not call designeted initilizer from designeted initilizer
 * Can not call convenience initlizer from designeted initilizer
 * If we use default init() then when we subclass it we do not need to write super.init() in Init() subclass but if we use like init(name: String then) then we need to write super.init(name: String) in subclass
 *A convenience initializer can be defined in a class extension. But a standard one - can not.
 * convenience initilizer call conveniance initilizer from same call but.
 * convenience initilizer must call designated initilizer otherwise it will through error
 
 ** Convenience initilizer provide flexibility to use some default feature
 */

class DesignatedConvenienceClass{

    var empName: String
    var empSalary: Double
    
    init(experienceEmpEmpName: String, empSalary: Double) {//designated
        self.empName = experienceEmpEmpName
        self.empSalary = empSalary
    }
      
    convenience init(freshersEmpName: String){//convenience
        self.init(experienceEmpEmpName: freshersEmpName, empSalary: 10000.0)
    }

    class func f11(){}//need to learn about class func
    
 

}


  class DesignatedConvenienceClass1: DesignatedConvenienceClass{

    
//    convenience  init(name1: String) {
//        super.init(name: name1)// cant call super init() from sub convenience initilizer
//    }
}

/*
 5. Failable
 * Failable initilizer is optional and it is use when we have chance  to get nil or empty and  not to match  requirement at that time failable will return the nil object
 * If the condition is failed then the failable initilizer will return nil object
 */
struct Animal {
    let species: String
    init?(species: String) {
        if species.isEmpty { return nil }
        self.species = species
    }
}


class FailableClass {

    let totalItems: Int
    let couponCode: String
    
    init?(totalItems: Int){
        if totalItems < 5 {
            return nil
        }
    self.totalItems = totalItems
    self.couponCode = "Use Coupon Code: iOS50"
    print(self.couponCode)
    }
}


class PropertyNatureOfClass{
        //*Classes don’t have a default memberwise initialize like var stringValue: String
//    var stringValue: String // It will through error(if we will even not initilied in init()) if we will not make it optional because class properties always required initialzer and does not have any default initializer.
    var optionalString: String?//optional does not requerd default initilizer
    var requiredString: String!//explicitly unwrap  does not requerd default initilizer
    
//    init() {
////        stringValue = "" //  with out optional and without using here it will through error
//    }
    
  
}


 class StaticClassFinal{
    
   final var finalVariable: String?
   static var staticVariable: String?
//   class var classVariable: String?

   final func finalF(){

    }
    class func classF(){

    }

    static func staticF(){

    }
    
    func f(){
        
    }
    
    init() {
        
    }
    
}

class SubStaticClassFinal: StaticClassFinal{
    override func f() {
        
    }
    override class func classF() {
        
    }
}

// INact private limited

