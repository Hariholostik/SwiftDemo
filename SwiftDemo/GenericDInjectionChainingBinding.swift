//
//  GenericDInjectionChainingBinding.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 1/12/21.


/*
Generic:- Generic is powerfull featurs in swift. Generic is used when we want to write code flexible and reusability function and types and that can accept any type.Using generic we can avoid the duplicate code. Swift inbuild type like arrya and dictionary also is a generic both the type accept any types.
 eg.
 *When user want just eneter the  two value and get the output in sum its means it may be Int or Double so we will create the generic func for this that will accept both the type Int or Double and get the sum of two value.
 * We can use the generic when we want two swape the two variable it may be Int or String.
 * Generic always return the same type as pass to generic function
 * if your generic function have two generic parameters then both the parameter accept similar data type value like Int,Int or String,String it can not accept Int,String or String,Int.
*/

/*
Extension: Extension add new functionality to an existing class,struct and enum or Protocol. Using Extension we can make clean or saperate code. Extension is used to implement the delegate. Even entension is used for any common functionality like change the placeholder color for UITextfield for whole UITextfield in porject then we create UITextfield extension  for change the color of placeholder.Extension doesnt have name.  We can create the extension for computed property for common use in project.
 */

/*
 Dipendancy Injection: DI is used to make our code loosely coupled for this our code is flexible and reusability of code.In Swift we can achive the  DI Using initializer init(pass the protocol).
 
 Initializer injection: provide dependency via the initializer init()
 Property injection: provide dependency via a property (or setter)
 
 *Tight Coupled: The class and object depend on one other due to this it reduce the flexibility and reusability of code.
 *Loose Coupled: Reducing the dependency of class on one other object we can get this using protocol.
 *Inversion of Control: Its a design principle, We can achieve this using DI.

 */



/*
 Optional Chaining: Optional chaining allows us to call property, method, subscript that may be nil.
 Optional Chaining is use to check whether the chain of optional variable has value or not. Optional chaining allows us to chain multiple optional together if any of chain optional has nil it will faild the conditon.
 Optional Chaining allow us to call property, method, subscript  that may be nil. If any of chain value return nil it will return the nil value.
 
 Optional Binding: Optional binding is used to check whether optional variable has value or not and using optional binding if optional variable has some value then it will store the that optional variable to other non optional variable it will implecitly unwrap the optinal variable.
*/


protocol AccountDetails {
    var userName: String{get}
    var password: String{get}
}

struct Gmail: AccountDetails{
    var userName: String{
        return "hg@gmail.com"
    }
    var password: String{
        return "Gmail@123"
    }
//    let userName = "hg@gmail.com"
//    let password = "Gmail@123"
}

struct Yahoo: AccountDetails{
    var userName: String{
        return "hg@gmail.com"
    }
    var password: String{
        return "Yahoo@123"
    }
//    let userName = "hg@yahoo.com"
//    let password = "Yahoo@123"
}
struct Outlook: AccountDetails{
    
    var userName: String{
        return "hg@gmail.com"
    }
    var password: String{
        return "Yahoo@123"
    }
//    let userName = "hg@outlook.com"
//    let password = "Outlook@123"
}


struct SelectedEmailAccountDetails{
    /*
     Tight Coupling
    var account: Gmail?
//    let account = Yahoo()
//    let account = Outlook()
        init(account: Gmail) {//Yahoo,Outlook//Dependency Injection
            self.account = account
        }
     func getEmailDetails()->Gmail{
         return account!
     }
     */
    
    /*
     Loose Coupling
     */
    
    
    
    
    var account: AccountDetails?
    init(account: AccountDetails) {// account now inverse the dependancy
        self.account = account
    }
    func getEmailDetails()->AccountDetails{
        return account!
    }
}


import UIKit

class GenericDInjectionChainingBinding: UIViewController{

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        genericfunc(valueA: 5, valueB: 5, valueAA: "Welcome", valueBB: "To")
//        getEmailAccountdetails()
        
//        optionalBinding()
        optonalChaining()
    }
    
//Generic
    func genericfunc<T: Strideable>(valueA: T, valueB: T, valueAA: Any, valueBB: Any)->T{
        print(valueA.advanced(by: 1))
        return valueA
    }
    
 //Dipendancy Injection
//*Initializer Injection(constructor) and property injection.
    
    func getEmailAccountdetails(){
        
        /*
         Tight Coupling
        let selectedEmailAccountDetails = SelectedEmailAccountDetails(account: Gmail())
        let selectedEmailAccountDetails = SelectedEmailAccountDetails(account: Yahoo())
        let selectedEmailAccountDetails = SelectedEmailAccountDetails(account: Outlook())
        let details = selectedEmailAccountDetails.getEmailDetails()
        print(details)
        */
        
        /*Loose Coupling*/
        let selectedEmailAccountDetails = SelectedEmailAccountDetails(account: Yahoo()) //account is now inject the dependacy
        let details = selectedEmailAccountDetails.getEmailDetails()
        print(details.userName, details.password)
    }
    
    
    
    func optionalBinding(){
        
        let firstName: String = "Holostik"
        let lastName: String =  "Ltd"
        var middleName: String?
            middleName = "India"
//          middleName = nil
        
    //Handling optional value using if else
    if middleName != nil{
        print("\(firstName) \(middleName!) \(lastName)") // if else statement explecitly unwrap the optional value
    }else{
        print("\(firstName) \(lastName)")
    }
    
        

    //Handling optional value using optional binding
        if let bindingMiddleName = middleName{  //optional binding implecitly unwrap the optional value.
            print("\(firstName) \(bindingMiddleName) \(lastName)")
        }else{
            print("\(firstName) \(lastName)")
        }
//        print(middleName)
//        print("\(firstName) \(lastName)")
    }
    
    
    func optonalChaining(){
        
        var brokers: Brokers? = Brokers()
//        brokers = nil
        brokers?.pgRooms = PGRooms()
        brokers?.pgRooms?.numberOfbeds = nil
//        print(brokers!.pgRooms!.numberOfbeds!)// it will crash the app
//        print(brokers?.pgRooms?.numberOfbeds)// it will not crash
        
        //chaining
        if let availableBed = brokers?.pgRooms?.numberOfbeds{
            print("number of: \(availableBed) are available")
        }else{
            print("beds are not available")
        }
        
        
        //bindings
        if let bindingbrokers = brokers{
            
            if let bindinPG = bindingbrokers.pgRooms{
                
                if let availablebindingBed = bindinPG.numberOfbeds{
                    print("number of: \(availablebindingBed) are available")
                }else{
                    print("beds are not available")
                }
                
            }else{
                print("pg not available")
            }
                                    
        }else{
            print("brokers not available")
        }
    
    }
}


class Brokers{
    var pgRooms:PGRooms?
}

class PGRooms{
    var numberOfbeds: Int? = 2
}

/*
n=3 ans= 3 // total number of hand shake by n people
n=4 ans= 6
 
 12...15,
 12 = 8,10,11,13,14
 
 !(12 % 2 == 0 || 15 % 2 == 0){
 }
 let andCon = 1&1
 print(andCon)
 67/1
33/1
16/0
8/0
4/0
2/0
[1100000]
[1110000]
 1100
 if 1 & 1{
 }else{
 }
*/
