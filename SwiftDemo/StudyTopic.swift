//
//  StudyTopic.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 17/02/22.
//

import Foundation
//public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
//public init?(coder: NSCoder)
//func encode(with aCoder: NSCoder) {
//  //add code here
//}
//required convenience init?(coder aDecoder: NSCoder) {
//  //add code here
//  self.init(title: "", rating: 0)
//}

//ClipToBound
//ClipToMask
//put,get,post
//crud rule-coredata,inverse
//Subscripts
//How coredata mantain atomicity multithreading
//how to handle the cuncurrency in insert and update
//app id vs app clip id
//IBDesignable
//IBInspectable
//wait/async
//NSCoding
//rest vs soap
// how to use Message Dispatch


/*
On demand
Core data version(old core data version handle) vs xcdata model vs xcdatamodelD
 //https://medium.com/@maddy.lucky4u/swift-4-core-data-part-5-core-data-migration-3fc32483a5f2
*/


/*
 why swift is protocol oriented programming or object oriented programming
 It can conform multiple protocols.
 It can be used by not only class, but also structures and enumerations.
 It has protocol extension which gives us common functionality to all types that conforms to a protocol.
 */
