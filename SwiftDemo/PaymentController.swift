//
//  PaymentController.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 9/02/22.
//


/*
 //https://payumobile.gitbook.io/sdk-integration/test-merchant-list
 PayU
 
 Salt
 Key: Merchant key received from PayU

 HashKey: sha512(key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||salt)
 
 */

/*
 In App Purchase
 
 1. Consumable
    * The product that use once after that it need purchase again and again like we have purchase 20 coin after used of 20coin we need to purchase again it.
 2. Non Consumable
    * The product need to purchase once after that it will not need to purchase again and again. like if you are purchase any educational video once it will not need to puchase again and again.
 3. Auto Renewable
    * The product will purchase for any duration after that it will need to puchase again and auto renewable automatically renew the plan.
 4. Non Renewing
    * Same as Auto Renewable but it will not renew automatically
 */
//https://medium.com/swiftcommmunity/implement-in-app-purchase-iap-in-ios-applications-swift-4d1649509599#:~:text=Now%20a%20days%2C%20most%20of,your%20iOS%20or%20MacOS%20applications.
//https://www.appcoda.com/in-app-purchases-guide/
//https://github.com/appcoda/In-app-Purchase-Game-Demo
import UIKit
import Foundation
import StoreKit

func getError()throws{ // throws indicate the id will give the error so we need to handle it using try
      // We can use try directly without using do catch when we use ? and ! with try? and try!
    // if we do not want direct call it we use  try with try catch
    //Error: error is a protocol
}

/*
 try? getError()
 try! getError()
 do{
     try getError()
 }catch{
     print(error)
 }
 */


protocol WhereProtocol {
    func addBorder()
}


extension WhereProtocol where Self : UIView{
    func addBorder(){
        layer.borderWidth = 2
    }

    
}

class WhereClass:UIView, WhereProtocol{

}

extension Array where Element == String{
     func getReverseArray()->[String]{
        return self.map{String($0.reversed())}
    }
}

enum ThrowsError: Error{
    case dataNotFound
    case serverError(msg: String)
}
class PaymentController: UIViewController {
        var optionalName: String?
        var price: String?
    func getUserName(name: String?)throws{
        
        guard let optionalName = optionalName else {
            throw ThrowsError.serverError(msg: "server error")
        }
        print(optionalName)
    
    }
    
    func whereWithArray(){
        
        let names = ["amar","raj","garv","rahul", "denny"]
        for name in names where name.contains("e"){
            print(name)
        }
    }

//    static public private(set) let useId = 100
    
//    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        whereWithArray()
        
        // use of try =======
        do{
            try getUserName(name: "" )
        }catch{
            print(error)
        }
        startObserving()
    }
    
 
    func img(url: URL, size: CGSize)-> UIImage?{
        
        guard let img =  UIImage(contentsOfFile: url.path) else {
            return nil
        }
        
        let render = UIGraphicsImageRenderer(size: size)
        
        let image = render.image { (context) in
            img.draw(in: CGRect(origin: .zero, size: size))
        }
        
        return image
    }
    
    @IBAction func inAppPurchaseAction(_ sender: UIButton){
        print("inAppPurchaseAction")
        configIAP()
    }
    
    
    func configIAP(){
        // Get the product identifiers.
        guard let productIDs = getProductIDs() else {
            return
        }

        // Initialize a product request.
        let request = SKProductsRequest(productIdentifiers: Set(productIDs))

        // Set self as the its delegate.
        request.delegate = self

        // Make the request.
        request.start()
        
        
    }
    
    
    // MARK: - General Methods
    
    fileprivate func getProductIDs() -> [String]? {
        
        guard let url = Bundle.main.url(forResource: "IAP_ProductIDs", withExtension: "plist") else {
            return nil
        }
        
        do {
            let data = try Data(contentsOf: url)
            
            let productIDs = try PropertyListSerialization.propertyList(from: data, options: .mutableContainersAndLeaves, format: nil) as? [String] ?? []
            
            return productIDs
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    
    func getPriceFormatted(for product: SKProduct) -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = product.priceLocale
        return formatter.string(from: product.price)
    }
    
    
    func startObserving() {
        SKPaymentQueue.default().add(self)
    }


    func stopObserving() {
        SKPaymentQueue.default().remove(self)
    }
    
    func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    
    
    // MARK: - Purchase Products
    
    func buy(product: SKProduct, withHandler handler: @escaping ((_ result: Result<Bool, Error>) -> Void)) {
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
    
    
    func restorePurchases(withHandler handler: @escaping ((_ result: Result<Bool, Error>) -> Void)) {
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
}


// MARK: - SKPaymentTransactionObserver
extension PaymentController: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        transactions.forEach { (transaction) in
            switch transaction.transactionState {
            case .purchased:
//                onBuyProductHandler?(.success(true))
                SKPaymentQueue.default().finishTransaction(transaction)
                
            case .restored:
//                totalRestoredPurchases += 1
                SKPaymentQueue.default().finishTransaction(transaction)
                
            case .failed:
                if let error = transaction.error as? SKError {
                    if error.code != .paymentCancelled {
//                        onBuyProductHandler?(.failure(error))
                    } else {
//                        onBuyProductHandler?(.failure(IAPManagerError.paymentWasCancelled))
                    }
                    print("IAP Error:", error.localizedDescription)
                }
                SKPaymentQueue.default().finishTransaction(transaction)
                
            case .deferred, .purchasing: break
            @unknown default: break
            }
        }
    }
    
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
//        if totalRestoredPurchases != 0 {
//            onBuyProductHandler?(.success(true))
//        } else {
//            print("IAP: No purchases to restore!")
//            onBuyProductHandler?(.success(false))
//        }
    }
    
    
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        if let error = error as? SKError {
            if error.code != .paymentCancelled {
                print("IAP Restore Error:", error.localizedDescription)
//                onBuyProductHandler?(.failure(error))
            } else {
//                onBuyProductHandler?(.failure(IAPManagerError.paymentWasCancelled))
            }
        }
    }
}




// MARK: - SKProductsRequestDelegate
extension PaymentController: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        // Get the available products contained in the response.
        let products = response.products

        // Check if there are any products available.
        print(products.count)
        if products.count > 0 {
            
            print(products.count)
            // Call the following handler passing the received products.
//            onReceiveProductsHandler?(.success(products))
        } else {
            // No products were found.
//            onReceiveProductsHandler?(.failure(.noProductsFound))
        }
    }
    
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
//        onReceiveProductsHandler?(.failure(.productRequestFailed))
    }
    
    func requestDidFinish(_ request: SKRequest) {
        // Implement this method OPTIONALLY and add any custom logic
        // you want to apply when a product request is finished.
    }
}


