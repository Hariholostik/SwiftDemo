//
//  ClosureDelegateController.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 18/10/21.
//

/*
Protocol: Protocal define blueprint of method and properties. Protocal is used when we have sets of common functionality used in project(sets of similar functionality used in different classes).
 //To keep simple if definition of function is different use Closures otherwise if its common and one want to reuse it go with Protocol.
 //https://www.youtube.com/watch?v=ZtU9OUJSMjA&t=61s
 Delegate:-
 //We can only able to use optional delegate with classs not with struct or enum beause when we mention @objc its means these protocol can we access by objective c as well so objective c only work with reference type.
 //We can only define weak delegate when inherit class/AnyObject(only referance type) keyword in protocol.
 //
 
 Closure:- Closure are self contain block of functionality. its used for callback. closure are referance type. Closure have thier own memory address.
 NonEscaping/sync/default: Its a default in closure and it is  used when we have light functionality in closure and its work like serial manner.
 Escaping/async: It is used when we have heavy functionality in closure its work like parallel manner. Generally escaping is work when task is taking time in closure like server api calling, storing closure in array and timer etc.When we called the escaping closure compiler just initialie the block and return from the block and escaping closure still in memory until and unless task will complete. If we will not use escaping closure like api calling it will hold the current thread till the api is successfully called.after returning the function it will still in memory...
 Autoclosure: Its call in one line and it is not proving callback scope or like {}//Autoclosure make hard to make code understand. We can not write multiline code after callback from closure only write the single line of logic.
 
 //https://learnappmaking.com/weak-vs-strong-references-swift/#whats-a-strong-reference
 CaptureList: Closure are reference type so when we need to create value type  then  we have to create capture list.
 //https://www.youtube.com/watch?v=7bb8mtrsj1Q
 //https://medium.com/swlh/using-capture-lists-in-swift-19f408f986d
 Capture lists: The list of values that you want to remain unchanged at the time the closure is created.
 Capture Value: When we use outside variable in closure and closure capture  that outsite variable its will even not release when outside variable no longer.
 */


/*
 //https://medium.com/@lovekeshbhagat23/ios-protocols-equatable-comparable-b85998fc9194
Equatable: Equatable protocol is used to check the equal between two instance of class(value or proprty not memory addess), struct using these keywords "==" and "!=".
 * We can compare instance(memory address) using "==="(with out using Equatable protocol)
 
Comparable: Comparable is same ad Equatable because It is inherited from Equatable but it has more flexibility using these keyword ">", ">=", "=>" , "<" and also "==", "!=".
 
Hashable: Hashble protocal is used to comapare the hash value which is the interger represant of any value(Its not guarantee to get hash value same every time). It is also Inherited by Equatable protocol.
*/



import UIKit

// Declare delegates method in protocol
/*
protocol Car{
    func applyBreaks()
    func start()
    func stop()
    func hornSound()
}


class RangeRover: Car{
    func applyBreaks() {
        print("RangeRover: break")
        // can be different break style as Audi
    }
    func start() {
        print("RangeRover: start")
    }
    func stop() {
        print("RangeRover: stop")
    }
    func hornSound() {
        print("RangeRover: hornSound")
        // can be different hornSound style as Audi
    }
}


class Audi: Car{
    func applyBreaks() {
        print("Audi: break")
        // can be different break style as RangRover
    }
    func start() {
        print("Audi: start")
    }
    func stop() {
        print("Audi: stop")
    }
    func hornSound() {
        print("Audi: hornSound")
        // can be different hornSound style as RangRover
    }
}



// car drive by person
class Person {
    
    func driveCar(car: Car){
       print("Person driving car")
        car.start()
    }
}
*/


 protocol Engine {
    func startEngine()
    func stopEngine()
}
@objc protocol SpeedControl {
    @objc optional func increaseSpeed()
                  func decreaseSpeed()
}

protocol Saftey {
    func releaseAirBags()
    func applyUrgentBreaks()
}

 protocol Car: Engine, SpeedControl, Saftey {
     func appleBreak()
}

class Audi: Car{
    func appleBreak() {
        print("Audi: appleBreak")
    }
    
    func startEngine() {
        print("Audi: startEngine")
    }
    
    func stopEngine() {
        print("Audi: stopEngine")
    }
    
    func increaseSpeed() {
        print("Audi: increaseSpeed")
    }
    
    func decreaseSpeed() {
        print("Audi: decreaseSpeed")
    }
    
    func releaseAirBags() {
        print("Audi: releaseAirBags")
    }
    
    func applyUrgentBreaks() {
        print("Audi: releaseAirBags")
    }
}


class RangeRover: Car{
    func appleBreak() {
        print("Audi: appleBreak")
    }
    
    func startEngine() {
        print("Audi: startEngine")
    }
    
    func stopEngine() {
        print("Audi: stopEngine")
    }
    
    func increaseSpeed() {
        print("Audi: increaseSpeed")
    }
    
    func decreaseSpeed() {
        print("Audi: decreaseSpeed")
    }
    
    func releaseAirBags() {
        print("Audi: releaseAirBags")
    }
    
    func applyUrgentBreaks() {
        print("Audi: releaseAirBags")
    }
}


class Person{
    func driveCar(car: Car){
        car.startEngine()
    }
}



/*
 Protocol Extension
 */
// We can achive optional functionality using Protocol extension
// Using protocol extension we can even implement with struct because optional only work with class.
protocol ProtocolExtension {
    func requiredProtocol()
}

extension ProtocolExtension{
    func optionalProtocol(){
        print("optionalProtocol")
    }
}



struct ProtocolExtensionClass: ProtocolExtension {
    func requiredProtocol() {
        print("requiredProtocol")
    }
}



/*
 I can say that swift protocols are more powerful than objective c coz you can add extensions, properties and associated types with swift but with objective c you have all the amazing restrictions and are just limited to reference types but in swift you can implement protocol in value and reference type both.
 */



class ClosureDelegateKVOKVC: UIViewController{

    var labelObj: UILabel?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        singleton()
//        let obj = B()
//        obj.all()
//        protocalCalls()
//        protocolExtn()
        
//        additionResultFromClosure()
//          closureArrayCall()
       
        /*
        let incrementByTen = makeIncrementer(forIncrement: 10)
        print(incrementByTen())
        print(incrementByTen())
        
        let incrementBySeven = makeIncrementer(forIncrement: 7)
        print(incrementBySeven())
        print(incrementBySeven())
        */
        
//        kvcFunction()
//        kvoFunction
        
        capturList()


    }
    
    
    func protocolExtn(){
        
        let protocolExtensionClass = ProtocolExtensionClass()
        protocolExtensionClass.requiredProtocol()
        protocolExtensionClass.optionalProtocol()
    }
    
    
    
    func protocalCalls(){

        let rangeRover = RangeRover()
        let audi = Audi()
        
        let person = Person()
        person.driveCar(car: audi)

//        rangeRover.applyBreaks()
//        rangeRover.start()
//        rangeRover.stop()
//        rangeRover.hornSound()
//
//        audi.applyBreaks()
//        audi.start()
//        audi.stop()
//        audi.hornSound()
        
        
    }
    
    func singleton(){

        let classInstance = SingletonClass.singletonClass
        print("Before class singleton change: \(SingletonClass.singletonClass.state)")
        
        classInstance.state = 20
        print("After class singleton change: \(SingletonClass.singletonClass.state)")


        var structInstance = SingletonStruct.singletonStruct
        print("Before struct singleton change: \(SingletonStruct.singletonStruct.state)")
        
        structInstance.state = 60
        print("After struct singleton change: \(SingletonStruct.singletonStruct.state)")
        print("New Object After struct singleton change: \(structInstance.state)")
        
        
    }
    
    
    func additionResultFromClosure(){
        
        print("Initial step")
        additionClosure(a: 10, b: 20) { [weak self] (result) in
            debugPrint("Result \(result)")
            
            guard let _ = self else{return}
            print(self?.view as Any)
            
            self?.labelObj = UILabel()
            self?.view.addSubview((self?.labelObj)!)
            
        }
        print("Final step")

    }
    
    @IBAction func buttonAction(_ sender: UIButton) {
        
        guard let _ = labelObj else {return}
        print(labelObj as Any)
    }
    
    func additionClosure(a: Int, b: Int, completionHandler: @escaping(_ result: Int)-> Void){
    // @escaping
        
        let result = a + b
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
          print("Callback step")
          return completionHandler(result)
         
        }
                
        /*
            Non-Escaping Steps:-
             Initial step
             Callback step
             "Result 30"
             Final step
        */
        
    /*
         Escaping Steps:-
         Initial step
         Final step
         Callback step
         "Result 30"
    */
        
}
    
    
    
    var closureArray : [()->Void] = []
    
    
    func closureArrayCall(){
        
        print("start")
        closureArrayF {
//            print("return")
            print("welcome")
        }
        print("final")
//        closureArray[0]()
    }
    
    func closureArrayF(handler :@escaping ()->Void){
//        print("handler")
        print("hello")
        closureArray.append(handler)
//        return handler()
    }
    
    
    let closure = {
        
    }
    
    
   // Capture List ==========================================
    
    
    func capturList(){
        
        
      /* var a = 5, b = 5
        let captureVariable: ()->() = { [a,b] in // here 'in' indicate to compiler that after 'in' it will capture the block. [a,b](array) it will capture the value of a and b.
            print(a,b)
        }
        
          captureVariable()
            a = 10; b = 10
          captureVariable()
            a = 15; b = 15
          captureVariable()
        */
        
        
        /*
        var  captureArray : [()->()] = []
        var indexValue = 0
        
        for _ in 1...5{
            
            captureArray.append { [indexValue] in
                print(indexValue)
            }
            indexValue += 1
        }
        
        
        captureArray[0]()
        captureArray[4]()
        */
        
        

        var str = "Hello, World!"
        var myClosure = { [str] in //capture list
        print (str)
        }
        str = "next"
        let inc = myClosure
        inc()
        
        
//        var str = "Hello, World!"
//        var myClosure = {    //no capture list
//        print (str)
//        }
//        str = "next"
//        let inc = myClosure
//        inc()
        
        
    }
    
    
    
   /*
    Important Note:
    This applies to value types and not reference types. Even when you use a capture list with a reference type both the inner and outer scope will refer to the same object due to reference semantics.
     */
    
    
    
    
    // Key Value Coading and Key Value Observer  ===================================================
    
    /*
     //https://mobikul.com/kvo-and-kvc-in-swift/
     //https://hackernoon.com/kvo-kvc-in-swift-12f77300c387
    //Key Value Coding: KVC allows you to access an object's properties indirectly, using strings to access them. Its only possible when we inherit the NSObject(NSKeyValueCoding protocol) to class and make the property to @Objc dynamic.  kvc.setValue("dehradun", forKey: "address").The working of the KVC is more like the Dictionaries.
     Key Value Coding is a coding machanishm to allow us to get or set any property value using string.
    //Key Value Obseving: KVO is technuie to observe the change in property or value or its a techniue to handle the state of property or value when it change with the help of KVC.
    */
    
    func kvcFunction(){
        let kvc = KeyValueCodngClass()
        kvc.address = "mdda"
        kvc.setValue("dehradun", forKey: "address")
        kvc.setValue("uk", forKeyPath: "keyValueCodng.address")
        
        print(kvc.address as Any)
        print(kvc.value(forKey: "address") as Any)
        print(kvc.value(forKeyPath: "keyValueCodng.address") as Any)
    }
    
    
    func kvoFunction(){
        
        var kvo = KVOClass()
        kvo.setValue("hp", forKey: "nameF")
        kvo.addObserver(self, forKeyPath: "nameF", options: [.new, .old], context: nil)
        Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { (Timer) in
            kvo.setValue("giri", forKey: "nameF")
        }
        
        Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false) { (Timer) in
            kvo.setValue("home", forKey: "nameF")
        }
    }
    
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "nameF"{
            print(change)
        }
    }

    
    // Equatable, Comparable, Hashable ====  ===== ==== ===
    
    
    
    func empEquatableComparableHashable(){
        
        //Equatable====
       // Class
        
        let empEquatableClass = EmpEquatableClass(empId: 10, name: "h")
        let empEquatableClass1 = EmpEquatableClass(empId: 10, name: "h")
        
        if empEquatableClass == empEquatableClass1{
            print(empEquatableClass)
        }
        
        // Struct
        let emp1 = EmpEquatable(empId: 101, name: "h")
        let emp2 = EmpEquatable(empId: 102, name: "g")
        let emp3 = EmpEquatable(empId: 103, name: "hh")
        let emp4 = EmpEquatable(empId: 104, name: "gg")
        let empArray = [emp1,emp2,emp3,emp4]
        
        let check = EmpEquatable(empId: 101, name: "h")
        
        if emp1 == check{
            print(emp1)
        }

        if empArray[0] == check{
            debugPrint("Index 0 = \(empArray[0])")
        }else if empArray[1] == check{
            debugPrint("Index 1 = \(empArray[0])")
        }else if empArray.contains(check){
            debugPrint("contains true")
        }else{
            debugPrint("contains false")
        }
        
        
        //Comparable
        let empComparableLHS = EmpComparable(age: 30, name: "h")
        let empComparableRHS = EmpComparable(age: 28, name: "g")
        if empComparableLHS < empComparableRHS{
            print(empComparableRHS)
        }
        
        
        //Hashable
        let empHashableLHS = EmpHashable(age: 10, name: "h")
        let empHashableRHS = EmpHashable(age: 10, name: "hh")
        
        
        if empHashableLHS.hashValue == empHashableRHS.hashValue{
            print(empHashableLHS)
        }

    }
    

}


class KeyValueCodngClass: NSObject {
    
   var name: String?
   @objc dynamic  var address: String?
   @objc dynamic var keyValueCodng: KeyValueCodng?
}


class KVOClass: NSObject{
   @objc dynamic var nameF: String?
}

// Associated Protocol========

protocol AssociatedProtocol {
    associatedtype customParam
    func associatedParam(first: customParam, second: customParam)
}
extension ClosureDelegateKVOKVC: AssociatedProtocol{
    
    typealias customParam = Int
    func associatedParam(first: Int, second: Int) {
    }
}

extension UIView: AssociatedProtocol{
    typealias customParam = String
    func associatedParam(first: String, second: String) {
    }
}

class Equl: Equatable{
    
    var _name: String
    init(name: String) {
        _name = name
    }
    
    static func == (lhs: Equl, rhs: Equl) -> Bool {
        return lhs._name == rhs._name
    }
    
    
}


/// Equatable, Comparable, Hashable========================
struct EmpEquatable: Equatable{
    var empId: Int
    var name: String
    
   static func == (lhs: EmpEquatable, rhs: EmpEquatable) -> Bool{
    return lhs.empId == rhs.empId && lhs.name == rhs.name
//    return lhs.empId == rhs.empId
    }

    
    
}

class EmpEquatableClass: Equatable{
    var empId: Int?
    var name: String?
    
    init(empId: Int,name: String) {
        self.empId = empId; self.name = name
    }
    static func == (lhs: EmpEquatableClass, rhs: EmpEquatableClass) -> Bool{
        return lhs.empId == rhs.empId && lhs.name == rhs.name
        }
}


struct EmpComparable: Comparable{
    static func < (lhs: EmpComparable, rhs: EmpComparable) -> Bool {
        return true
    }
    var age: Int
    var name: String
    
    static func ==(lhs: EmpComparable, rhs: EmpComparable) -> Bool {
        return true
    }
}


struct EmpHashable: Hashable{
    
    var age: Int
    var name: String
    
    var getAge: Int{
        return age.hashValue
    }
    
    var getName: Int{
        return name.hashValue
    }
    
    static func == (lhs: EmpHashable, rhs: EmpHashable) -> Bool {
        return lhs.age.hashValue == rhs.age.hashValue
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.age)
//        hasher.combine(self.name)
    }
    
}
