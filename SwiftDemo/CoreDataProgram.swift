//
//  FirstScene.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 12/08/21.
//



import UIKit
import Foundation
import CoreFoundation
import CoreData

//https://ali-akhtar.medium.com/mastering-in-coredata-part-3-coding-crud-in-core-data-b7a278436c3

class CoreDataProgram: UIViewController {
    
    var managedContext: NSManagedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .red
        print("FirstScene")
        
       guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        managedContext = appDelegate.persistentContainer.viewContext
        print(appDelegate.persistentContainer.persistentStoreDescriptions)

//        getCoreDataDBPath()
                
    }
    
    
    func getCoreDataDBPath() {
        
            let path = FileManager
                .default
                .urls(for: .applicationSupportDirectory, in: .userDomainMask)
                .last?
                .absoluteString
                .replacingOccurrences(of: "file://", with: "")
                .removingPercentEncoding

            print("Core Data DB Path :: \(path ?? "Not found")")
        }
    
    
    @IBAction func insertAction(_ sender: UIButton) {
        createData()
    }
    
    @IBAction func retrieveAction(_ sender: UIButton) {
        retrievedData()
    }
    @IBAction func updateAction(_ sender: UIButton) {
        updateData()
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        deleteData()
        
    }
    
    
    func createData(){
        
        let userEntity = NSEntityDescription.entity(forEntityName: "UserEntity", in: managedContext)
        
        let teacherEntity = NSEntityDescription.entity(forEntityName: "Teacher", in: managedContext)

        for i in 1...2{
            
            let user = NSManagedObject(entity: userEntity!, insertInto: managedContext)
            
            let teacher = NSManagedObject(entity: teacherEntity!, insertInto: managedContext)
                teacher.setValue("Teacher\(i)", forKey: "tName")
                teacher.setValue("Class\(i)", forKey: "tClass")

                user.setValue("Holostik\(i)", forKey: "name")
                user.setValue("Holostik\(i)@gmail.com", forKey: "email")
                user.setValue(15+i, forKey: "age")
                user.setValue(teacher, forKey: "teacher")
        }
        
        do{
            try managedContext.save()
        }catch let error as NSError{
            print("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
//    open func undo()
//
//    open func redo()
//
//    open func reset()
//
//    open func rollback()asha
    
    
    func retrievedData(){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserEntity")
        
//        fetchRequest.fetchLimit = 2
//        fetchRequest.predicate = NSPredicate(format: "name = %@", "Holostik2")
//        fetchRequest.sortDescriptors = [NSSortDescriptor.init(key: "email", ascending: false)]
        
        do{
            let result = try managedContext.fetch(fetchRequest)
            
            for data in result as! [NSManagedObject]{
                
                
                let teacherData = data.value(forKey: "teacher") as! NSManagedObject
                
//                print(data.value(forKey: "name") as! String)
                print(teacherData.value(forKey: "tClass") as! String)

            }
            
        }catch{
            print("failed")
        }
    }
    
    
    func updateData(){
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "UserEntity")
        fetchRequest.predicate = NSPredicate(format: "name = %@", "HolostikUpdated")
        
        do{
            
                let result = try managedContext.fetch(fetchRequest)
                
                let objectUpdate = result[0] as! NSManagedObject
                objectUpdate.setValue("HolostikUpdated", forKey: "name")
                objectUpdate.setValue("HolostikUpdate@gmail.com", forKey: "email")
                objectUpdate.setValue(100, forKey: "age")
                do{
                    try managedContext.save()
                }catch{
                    print(error)
                }
        }catch{
            print(error)
        }
        
    }
    
    
    
    func deleteData(){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserEntity")
        fetchRequest.predicate = NSPredicate(format: "name = %@", "Holostik1")
        
        do{
            
             let result = try managedContext.fetch(fetchRequest)
            
              let objectToDelete = result[0] as! NSManagedObject
              managedContext.delete(objectToDelete)
            
            do{
                try managedContext.save()
            }catch{
                print(error)
            }

            
        }catch{
            print(error)
        }
    }
    
    
    @IBAction func buttonAction(_ sender: UIButton) {
        didTapCat()
    }
    func didTapCat() {
        
            let activity = NSUserActivity(activityType: "SecondScene")
            //Add targetContentIdentifier
        
            activity.targetContentIdentifier = "new"
        
            let session = UIApplication.shared.openSessions.first { openSession in
                
                guard let sessionActivity = openSession.scene?.userActivity, let targetContentIdentifier = sessionActivity.targetContentIdentifier  else {
                return false
                }
                
            return targetContentIdentifier == activity.targetContentIdentifier
                
            }
        
            UIApplication.shared.requestSceneSessionActivation(session, userActivity: activity, options: nil, errorHandler: nil)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
