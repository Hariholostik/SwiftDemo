//
//  SDLC.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 10/01/22.
//

import Foundation

//SDLC:


/*
 Water Fall
 * Requirements,Design,Development,Testing,Deployment, Maintenance
 
 1. Waterfall model issues
 -Late feedback from Client
 -Can not take care of the Change Request
 -one after another process without increaments
 */


/*
 Aglie
 
 2. Agile Development Process:
 -takes early feedback from client
 -Can take care of the Change request
 -Increamental flow, not all at once
 -Iterative flow, project made in steps
 -daily review
 -self organizing: Team collaboration
 -Team size is small 4-5


 Iterative (Steps/ no of sprints:)  and Increamental (workable product/ Module)

 Agile flow
 1. Product Backlog (Collection of user Stories + proriity): (SOW->Product Specification/ Vision->Product Description->Product/ Module (User Stories)
 2. Sprint Backlog (part of Product backlog, taken 3-4 product at a time )
 3. Sprints (1 week to 1 month); Dev (4)+ Testing (4) + Defect fixing (2)- (2 weeks sprint/ 10 days)
 4. Final product ->UAT


 Agile Framework
 1. Scrum

 Scrum Roles
 -product Owner
 -Scrum Master
 -Scrum Team

 Scrum Framework
 1. project Vision with Client and Product owner
 2. Product owner created product backlog (covering all user stories) with priority
 3. Sprint Planning Meeting (all members) : What to be made, how much time (Output: Sprint Backlog)
 4. Daily Standup meeting (scrum master, scrum team-working on sprint, Project Manager): What we did yesterday, what we will do today, any issues
 5. Sprint Review meeting (scrum master, scrum team-working on sprint, Project Manager, Product Owner): Review workable module/ product/ shippable product
 6. UAT (Sprint Release)
 7. Sprint Retrospective meeting (scrum master, scrum team-working on sprint, Project Manager, Product Owner): What we leanrt, what we did wrong, where to improve
  

 link: https://www.tuleap.org/agile/agile-scrum-in-10-minutes



 2. Kanban
 -uses Kanban board
 -To do, In Progress, Done
 -Can be used for product backlog, sprint backlog
 */



//Jira
//CICD

//OS Layer(work)
/*
 *Cocoa Touch Layer: UIKit, Storyboard, Pushnotification
 *Media Layer: AVFoundation,Core Audio, Core Image,Core Animation
 *Core Services Layer: Foundation,Core Location, Core Data, Contact Address, System Configuration
 *Core OS Layer: Hardware Connectivity like bluetooth, Core Security
 */

//IPC(inter process communication)
// Deeplinking
