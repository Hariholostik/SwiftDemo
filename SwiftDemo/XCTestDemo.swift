//
//  XCTestDemo.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 2/02/22.
//

import Foundation

protocol XCTestProtocol{
    func additionOfTwoNo(numberOne: Int, numberTwo: Int)->Int
    func f1()
}

extension XCTestProtocol {
    func f1(){
        print("f1")
    }
}

class ServiceHandle: XCTestProtocol{
    func additionOfTwoNo(numberOne: Int, numberTwo: Int) -> Int{
        return numberOne + numberTwo
    }
}




