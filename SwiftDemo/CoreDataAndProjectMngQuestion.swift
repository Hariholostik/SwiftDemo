//
//  Question.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 7/09/21.
//

import Foundation

/*
 . Time estimation(how to calculate)
 .
 */


/*
Project:
 1.SOW(scope of work): requirement,details about client, about project,about module(taking approval from client),
 2. kick of meeting(with sales peroson)- scope, already had similar project ,
 3. SRS (Software requement spesification): Whole details about project with screens and details about modules, all description(taking approval from client)
 4. Design Documents: table schema, describe every button click event work (taking approval from client)
 
 5 PMP(Project managment plan): it include every think like sow, srs and client communication(when meeting with whome), giving status of client about project status by weekly or monthly
 
 6. Size Estimation: we can devide whole project  time in module like code 100hr, testing 30hr.
 7. WBS(Work breakdown structure): We can devide module in submodule like coding- splash screen 3hr, login 5hr.
 
 8.Working on project and after completed project tester will do system testing.
 
 9. intregation testing:  test after  whole project completed
 
 10. UAT(User acceptance testing): after complted the porject we will give the project to client for UAT.
 
 */


/*
 Scrum:
Agile:
 * Meeting with PM regarding sprint planing
 *Sprint(chunks): work schedule of team like start date and end date of any module (development testing, defact resolved)
 
* after completed chunks we will  do unit testing then we will give for testing(System testing)
* gets defect from tester and again work on defect()
* intregation testing:  test after  whole project completed.
*/


/*
 Waterfall model:
 * first completed all project then we will do unit testing then we will give for testing.
 *  droback: if any bugs come after complted the project then we have to go through the whole project and it will take times.
 */

/*
 Unit Test:
 https://www.tutorialspoint.com/software_testing_dictionary/unit_testing.htm
 */
/*
CI/CD:
*/


/*
 https://stackoverflow.com/questions/33560722/why-can-we-use-a-nsmanagedobjectcontext-without-setting-its-persistentstorecoord
 https://cocoacasts.com/six-common-questions-about-core-data
 
 https://ravindrabhati919.medium.com/40-coredata-interview-questions-and-answers-3ff345c90347
 
 Core Data- sql lite, xml, binary, in-memory
 NSManageObjectModel:- Its a collection of entity,attribute and relationship or schema. Its a represantation of the xcdatamodel file. Its responsibility to mapped the entity and attribute with presestant store.
 
 NSManagedObjectContext:- its reposnibility to  manage the  collection of manage object and pass manage object to persistent store. Using manageobjectcontext we can manipulate the data like insert delete update, retrive etc. NSManagedObjectContext store data in cache if we will not excute save function. Whenever we will retrive data from  persistent store manageobject alws check first if that date available in cache or not if available then it will return data from cache.
 
 NSPersistentStoreCoordinator: its responsibility to communicate manageobjectcontext and persistent store to comunicate when NSManageObject data to manupilate in any data.
 
 NSPersistentStore: its responsibility to store NSManageObject data.
 
 NSPersistentContainer/CoreDataStack: Its a collection of model,context,cordinator and store. its encapsulate the core data stack in application.
 * persistent container is responsible for creating and managing the coredatastack
 
 NSManagedObject: its a represantation of core data entity its same as dictionary and we can get its data using key valu coding
 
 
 Fetched Properties: Using fetched property we can get data in the from of array from predicate using queary. we will use fetched property to get array data using queary.
 
 
 NSFetchRequest: Its used to fetch or update the data from persistent store
 NSBatchUpdateRequest: Its fastter then NSfetchRequest and its directly communicate with pesistent store and manupalate the attribute.
 
 1. Perform: async, cuncurrent queue
 2. PerformAndWait: syn, serial queue
 
 ConcurrencyType:-
 case confinementConcurrencyType = 0
 case privateQueueConcurrencyType = 1
 case mainQueueConcurrencyType = 2
 
 */

/*
 https://www.youtube.com/watch?v=-OMPCSwEuHs
 Deletion Rule-
 UserTable and OrderTable
 No Action: Deleted user from UserTable but not deleted user order from OrderTable
 Nullify: Deleted user from UserTable but not deleted user order from OrderTable and replace user id to Null
 Cascade: Deleted user from UserTable as well as deleted user order from OrderTable
 Deny: User Can not deleted when some order is on processing state
  */


/*
Team Id: It is provided by the apple and it is uniquely identify the apple developer account.
Bundle Id: It is uniquely identify the app.
App Id: It is a combination of Team Id and Bundle Id. It is use when create the provisioning profile and it will identify the provisioning profile in keychain and Xcode.
*/
