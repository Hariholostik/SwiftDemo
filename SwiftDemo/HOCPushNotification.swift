//
//  HOCPushNotification.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 8/12/21.
//
//https://levelup.gitconnected.com/higher-order-functions-in-swift-35861620ad1
//https://medium.com/@Dougly/higher-order-functions-in-swift-sorted-map-filter-reduce-dff60b5b6adf
//https://abhimuralidharan.medium.com/higher-order-functions-in-swift-filter-map-reduce-flatmap-1837646a63e8

/*
 HOF: Higher Order function is simple function that  take one or more function as parameter or argument and return  result as in function.
 */
//Sorted, Map, Filter, Reduce
// forEach, map, CompactMap, flatMap, filter, reduce, sort, and sorted.
/*
  Need Complexcity of all HOF
  ForEach: We use ForEach  to iterate the element from array. We can not use break and continue in ForEach loop to exit from colosure.
  Sorted: It will sorted the array and return the new array accroding to your condition for accending or decending.
  Sort: It will sort the same array and no need to created new array
  Map: We use map to iterate the element from array and will return the updated array. We do not need to manually created new array object and added the updated element in new created array using for in loop.
  Filter: We use the filter to iterate the element from array and will return the these element in updated array which is satisfied the condition.
  Reduce: We use the reduce to iterate the element from array and will combine, adding or do other logic with all array element and return the object (in generic).And we have initial value to start it.
  Flatmap: We use flat map to convert any thing like string, string array, 2d array and  sets in the from of  array then we use flatmap. flat map is also used to remove the nil now its depracated instead use compactMap.
  CompactMap: We use compactMap in collection type whose is containing  the nil value and it will return non nil array if we use array, set never accept the nil value and dictionary will return the array of tuple with key value.
      and converted like int to double.
 //'flatMap' is deprecated: Please use compactMap(_:) for the case where closure returns an optional value
 
  Set: It can not accept the duplicate value and cant accept the nil value. we can apply set on union intersaction etc.
 */


/*
 //https://medium.com/swift2go/ios-ad-hoc-distribution-33ecabbadb4e
Team Id: It is provided by the apple and it is uniquely identify the apple developer account.
Bundle Id: It is uniquely identify the app.
App Id: It is a combination of Team Id and Bundle Id. It is use when create the provisioning profile and it will identify the provisioning profile in keychain and Xcode.
*/

/*
 //https://stackoverflow.com/questions/56641826/difference-between-explicit-and-wildcard-while-creating-app-id
 //https://stackoverflow.com/questions/1692320/when-to-use-an-explicit-app-id-versus-a-wildcard-id
 WildCard: we chosse wildcard when we dont want to use push notification and in app purchase thing and provided the free app to user then choose wildcard.
 using wild card we can share same provision profile for different bundleID
 like com.dummy.* we can use like com.dummy.demo and com.dummy.new
 */

/*
 Entitlement: It is created when we enable the any target from capability. Entitlement tell  the provision profile i am using this service.
 */
/*
 Notification:
 SSL: Secure Socket Layer
 CSR: Certificate Signing Request: Created the CSR from keychain and save it to desktop.Then upload CSR file to developer account certificate section for developement and push notification and download it as .cer and double click on file then it will visible in keychain.
 * We edit the bundle id then select capabilities then select push notification section and downloads push certificates.
 * We need to create saparate certificate for push notification for every app.
 * DeviceToken length is 32.
 * Max Payload size of push notification 2kb after ios 8 before 256bytes.
 There are 3 types of Push:-
 
 Push Notification/Normal:
         
         {
           "aps": {
             "alert": {
             "title": "Breaking News!",
             "subtitile": "sub Title",
              "body": "body"
           },
             "sound": "default",
         }
         }
 
 ===================================================
         {
           "aps": {
             "alert": "Breaking News!",
             "subtitile": "sub Title",
              "body": "body",
             "sound": "default",
         }
         }
 
 
 Silent Push Notification: We need to enable remote notifications from Capaility> Background Modes> Remote Notification.
            "content-available": 1
             {
               "aps": {
                 "content-available": 1
            },
                "data1": "",
                "data2": ""
             }

 
 Rich Push Notification/Media: Notification Content Extension vs Notification Service Extension
            "mutable-content": 1
             {
               "aps": {
                 "alert": {
                   "title": "",
                   "subtitle": "",
                   "body": ""
                 },
                 "mutable-content": 1
               },
               "podcast-image": "https://koenig-media.raywenderlich.com/uploads/2016/11/Logo-250x250.png",
               "podcast-guest": "Antonio Leiva"
             }
             
 */
/*
 * go to file >target > select > Notification Service Extension or Notification Content Extension
 Notification Service Extension:  Use for view image and other default action.
 Notification Content Extension: User to create custom UI like UIImageView, UIButton.
 
 */

import UIKit
class HOCPushNotification: UIViewController{
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        hofForEach()
//        hofMap()
//        hofFilter()
//        hofReduce()
//        hofFlatMap()
//        hofCompactMap()
//        hofSortedSort()
        hofExample()

    }
    
    
    func hofForEach(){
        
        let numberArray = [1,2,3,5,6,8]
        
        //for in
        for number in  numberArray{
            print(number)
        // you can use the break and continue in for in loop
        }
        print("\n")
        
        //forEach
        numberArray.forEach { (number) in
            print(number)
            // you can not use the break and continue in foreach loop to exit from closure.
        }
        
        print("\n")

        //Short forEach
        numberArray.forEach{
            print($0) //$0 means shortcut first argument in closure. $0 and $1 are Closure’s first and second shorthand arguments
        }
    }
    
   
    
    func hofMap(){
        
        let numberArray = [1,2,3,4,6,8]//[[1,2,3,4,5],[6,7,8,9]] //"hari"
        var updatedNumberArray: [String] = [] // manually created the array
        
        //for in
        for number in numberArray{
            updatedNumberArray.append("\(number)0") // manually added the updated element in array
        }
        print(updatedNumberArray)
        print("\n")

        //map
        let updatedMapArray = numberArray.map { (number) -> String in // return the updated array
            "\(number)0" //just update the element  of array and no need to manually added the element in array
        }
        print(updatedMapArray)
        
        print("\n")

        //short map
        let updatedMapShortArray = numberArray.map { // return the updated array
            "\($0)0" //just update the element  of array and no need to manually added the element in array
        }
        print(updatedMapShortArray)
    }
    
    
        func hofFilter(){
           
            let numberArray = [1,3,6,2,4,8]
            
            //filter
            let filterArray = numberArray.filter { (number) -> Bool in
                print(number)
                return number < 4 // only return the these element which is satiesfied the the condition in array.
            }
            
            print(filterArray)
            print("\n")
            
            //short filter
            
            let shortFilterArray = numberArray.filter {
                return $0 < 4
            }
            print(shortFilterArray)

        }
    
    
     func hofReduce(){
        
        
                let numberArray = [1,3,6,2,4,8]
                
                //Reduce
                let reducerObject = numberArray.reduce(0) { (result, number) -> Int in //
                    print(result)
                    return result + number // combine all array element and return object.
                }
                print(reducerObject)
                print("\n")
                
                let initialValue = "Start"
                let reducerStrObject = numberArray.reduce(initialValue) { (result, number) -> String in // initial value is start points and result hold the final value and number hold the current iterate value in array
                    return result + String(number)
                }
                print(reducerStrObject)
                print("\n")
                
                //short Reduce
                let shortReduceObject = numberArray.reduce(0) { // it start with some element like 0
                    return $0 + $1 // it add all array element and return the final out in object(generic) from
                }
                print(shortReduceObject)
            }
    
    
    
        func hofFlatMap(){
            
            
            let numberArray = [[1,3,5,7,9],[2,4,6,8,10]]
            
            //flatMap
            let flatMapArray = numberArray.flatMap { (number) in
                number //convert the 2d array to one d array
            }
            print(flatMapArray)
            print("\n")
            
            //short flatMap
            let flatmapShortArray = numberArray.flatMap {
                $0
            }
            print(flatmapShortArray)
            print("\n")

            //flat map with map
            let flatmapWithMapShortArray = numberArray.flatMap {
                $0.map { (value) -> Int in
                    value + 10
                }
            }
            print(flatmapWithMapShortArray)
            print("\n")
            
            let stringSeries = "Holostik India"
            let stringArray = stringSeries.flatMap { (stringValue) in
                stringValue
            }
            print(stringArray)
            print("\n")

            let stringSetArray = ["ABC", "DEF", "GHI"]//
//            let stringSetsArray = [["ABC", "DEF", "GHI"],["abc", "def", "ghi"]]//
//            let stringDict = [["key1": "ABC", "key2": "DEF", "key3": "GHI"]]//
            
//            let sets = [Set([1.0,2.0,3.0]), Set([4.0,5.0,6.0])]

            

            let stringSetOutArray = stringSetArray.flatMap { (stringValue)in
//                stringValue.map {
//                    $0.uppercased()
//                }
                stringValue
            }
            print(stringSetOutArray)
            print("\n")
            
        }
    
    
        func hofCompactMap(){
            
            let numberArray = [1,2,nil,3]
            
            print(numberArray)

            let compactArray = numberArray.compactMap { (number) in
                number // it will only return the non nil array
            }
            print(compactArray)
            print("\n")
            
            
            let dict = ["a": 1, "b": 2, "c": nil]
            
            let compactDict = dict.compactMap { (key, value) in
             
                (key,value) // it will return the array of tuple with key value
            }
            print(compactDict)
            print("\n")

        }
    
        
        func hofSortedSort(){
            
            let numberArray = [1,2,4,5,3]
            
            print(numberArray.sorted()) //accesnding order default
            
            let sortedArray = numberArray.sorted { (a, b) -> Bool in
                a > b // 1>2 // desending order
            }
            print(sortedArray)
                        
            var numberSortArray = [1,2,4,5,3]
             numberSortArray.sort { (a, b) -> Bool in
                a > b
            }
            
            numberSortArray.sort {
               $0 < $1
           }
            print(numberSortArray)
        }
        
    
        func hofExample(){
            
            let numberArray = [1,2,3,4,5,6,7,8]
            let primeArray = numberArray.filter {
                 $0 == 2 ?  true  : $0 % 2 != 0// 2 is also prime
            }
            
            print(primeArray)
            
            //flatmap,map,filter,reduce
            
            let twoDArray = [[1,2,3,], [4,5]]
            
            //1,2,3,4,5 //flatMap
            //2,3,4,5,6 //map
            //2,4,6 // filter
            //12
            let result = twoDArray.flatMap{$0}.map{$0 + 1}.filter{$0 % 2 == 0}.reduce(0){$0+$1}
            print(result)
            
            
            let n = readLine()
            
            let nn = Int(n!)
            
            
            var ary: [Int] = []
            for i in 0..<nn! {
            // arry.append(i)
            }
        }
    
    }

/*
*/
 
//9.6// 16

//5k jeevanlap
















