//
//  AppDelegate.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 12/08/21.
//

import UIKit
import CoreData
import UserNotifications// use for normal notification
import UserNotificationsUI// use for custom notification


//@objc protocol MyProtocol {
//   @objc optional func myMethod()
//}


class KeyValueCodng: NSObject {
    
   var name: String?
   @objc dynamic  var address: String?
    @objc dynamic var keyValueCodng: KeyValueCodng?
}



//hp
/*
 Question:
 //https://docs.swift.org/swift-book/ReferenceManual/Attributes.html
 deadlock
 9.encapsulatoin vs abstraction
 10.  Async/Await, Actors and Sendable, Mirror //swift5.5
 13. Singleton and static
 16.git //https://www.atlassian.com/git/tutorials/saving-changes/git-stash// stach
// https://abhimuralidharan.medium.com/higher-order-functions-in-swift-filter-map-reduce-flatmap-1837646a63e8
 18. Lazy https://abhimuralidharan.medium.com/lazy-var-in-ios-swift-96c75cb8a13a
 19. marge,bubble,binary,linklist//http://www.btechsmartclass.com/data_structures/introduction-to-algorithms.html
 */


/*
 
 how optional worl behind the scene
 
 let name = OptionalEnum<Any>.none
//        print(name)
   name.printName("non optinal")
   name.printName(nil)
 
 
 enum OptionalEnum<T>{
     
     case some(T)
     case none
     
     func printName(_ name: String?) {
         switch name {
         case .some(let unwrappedValue):
             print("Name is \(unwrappedValue)")
         case .none:
             print("Name is nill")
         }
     }
     
 }

 */


/*
 
 Result.Type = Result type is used to handle the succes and failure in better way
 like we will create the enum of ResultType with Success and Error and error has multiple type.
 
 enum ResultType<T>{
     case Success(T)
     case Error(ErrorType)
 }
 enum ErrorType: Error{
     case DataNotFound
     case WrongJson
     case InternetConnectionError
 }
 
func getStatus(str: String)->ResultType<String>{
     return ResultType.Success("api success")
     return ResultType.Error(.DataNotFound)
     return ResultType.Error(.InternetConnectionError)
 }
 */


/*
 Controller life cycle
 https://www.youtube.com/watch?v=d7ZqxvbiTyg
 */

/*
 Objective-C uses the term “protocol” instead of “interface,” and protocols can require methods and properties, but only a class can adopt a protocol, not a struct.
 
 Objective c: Only Class
 Swift: Class, Struct, Enum, Extension,  Actor
 */


/*
 https://medium.com/@sdrzn/functional-programming-in-swift-221a8cabb8c#:~:text=In%20terms%20of%20Swift%2C%20functional,to%20understand%20than%20imperative%20code.
 
 //https://www.youtube.com/watch?v=44ObbKc6ric
 
 
 Functional programming: When we use functional programming it means we are dealing with pure functiona and thread safe and its always immutable function.
 Functional programming is a programming paradigm in which we try to bind everything in pure mathematical functions style.
 *A key practice in functional programming is breaking code down into smaller or functional programing is a pradim.
 functional programming — using higher order functions to solve complex problems.
 
 // Normal function and not thread safe,any one can change the value of a and b
 
 var a= 10
 var b = 20
 func add()-> Int{
 return a+b
 }
 // Functional programming its thread safe and pure function and its thread  safe
 
 func add(x: Int, y: Int)->Int{ //pure function and can not modify x and y its immutable  function
     return x+ y
 }
 ** if we dealing with functional programming its means that we are in thread safe or immutable like .map,.reduce.. etc are thread safe.. we are always remove mutabilitty  its work with recursion not loop like HOC
 */


/*
 Swift nil is not the same as Objective-C nil:
 Swift’s nil is not the same as nil in Objective-C. In Objective-C, nil is a pointer to a non-existent object. In Swift, nil is not a pointer—it is the absence of a value of a certain type. Optionals of any type can be set to nil, not just object types
 // Objc:
 Object *foo = nil;
 foo = [Object new];
 foo = nil;

 // Swift
 var foo: Object = nil // not allowed
 var bar: Object = Object()
 bar = nil // not allowed

 var foo: Object? = nil // fine
 foo = Object()
 foo = nil
 */




/*
 Frame: When we use frame it represent the origin(x,y) from super view. if you rotate the view the (x,y) and (height,width) will change
 Bound: When we use bound it represent the origin(x,y) from own coordinate system. if you rotate the view the (x,y) will always same(0,0) and (height,width) will change
*/




/*
 self: It represent the current object.
 Self: It represent the current type it is use as placeholder of other type like Numeric is protocol it accept Int and double when you want return the value from protocl it may be Int or double at that time we use Self which is accept both the type //class.type(work with extension and protocol).
 
 extension Numeric{
     func squared()-> Self{
         return self + 4
     }
 }

*/


/*
 Defer: we can use defer to write block of code it is execute after all code of function and it is even execute after the return(function is exit) is called
*/


/*
 Static Function: It is directly call by class name no need to create the object for call and can not override the static function in subclass
 * Directly call by class name.
 * Function and variable can be static
 * Can not subclass it
 
 Class Function: Its is same as static function  but we can override the class function in subclass. can not create class variable.
 * Directly call by class name.
 * Function and class can be Class.
 * Can subclass it
 
 Final Function: If create class or function we can not subclass it and it need to create object to call it.
 * Can not directly call by class name it need to create object.
 * Function, class and variable can be Final.
 * Can not subclass it
 */


/*
 Architecture Pattern: It decide the folder structure in  ptoject and what is the role and responsibility of these files which is inside the folders. MVC, MVVM,MVP, VIPER etc are  Architecture pattern.
 
 Disign Pattern: Design pattern is used to provide the structure for  code in project or to resolve the specific problem we use design pattern. Creational(Factory,Singleton,Prototype,Builder,Abstract Factory, Object pool), Structural,Concurrency,Behavior(Observer).
 */

//https://yuvrajkale.medium.com/mvc-vs-mvvm-in-swift-addbceac8b3c
//MVC: View controller will very lengthy when we put like code, validation, server code.
//MVVM: View controller will not length  because we will create saparate model view for validation api call etc easly for Unit test.

/*
 advantage:
 * Less no of code in viewModel
 * Easly for Test Driven Development - TDD
 * Code Segregated
 * avoid bulky controller
 
 disadvantage:
 * number of class not easly to handle it
 * Not easly to debbuge- like you need to carefully when you do debbuge because you we have face number of class at for this its not easy
 * Communicating with various mvvm component and bindg data is not easy
 *Code reusability of view and view model is not easy - like if we want to some view and we have to use all the view model for this view and can to resue any saparate from view and view model
 * MVVM for beginners is hard to put to use.
 
 
 Basic thumb rules of MVVM:
 View Model is owned by the view and model is owned by the view model.
 View Model is responsible only for processing input to output and logic required for driving the UI.
 View Model should not modify the UI.
 The view should only be responsible for UI handling.
 The view should not interact with Data Model or vice-versa.

 */

//Singleton pattern: Singleton is used when we need to access share data in whole app  without  creating new object.
//Factory Design Pattern:

//Decomposition: In decomposition we will break the complex module in small module or  we will break complex module into multiple layer for single work.

/*
//Enum:  Enumerations are lists of things, enum are value type.
 CaseIterable: its a protocal. we can access collection of all type casses using allCases Property.
 
 hash value: hash value is integar represantation of some other value or hash value represent the  other value . But now hash value give every time different value.  now  hash value is deprecated.
 raw value: raw value represent the index  like in enum list(like and value represted in string).
 associate value: associate value allows us to add additional information at runtime and dynamically generatally enum values are define at compile time.
 * can no to set raw value and associate value in same enum
 associated type: An associated type gives a placeholder name to a type that’s used as part of the protocol. The actual type to use for that associated type isn’t specified until the protocol is adopted.
 
 //row value
 enum RowValueColor: String{
     case white = "#000000"
     case black = "#ffffff"
 //    case custom(hex: String)// associated value is hex
 }

 //associated value
 enum AssociatedValueColor{
     case white
     case black
     case custom(hex: String)// associated value is hex
 }
*/



/*
 //https://learnappmaking.com/nil-coalescing-ternary-conditional-operator-swift/
The Nil-Coalescing ?? and Ternary Conditional :? Operators in Swift
 ??- it is used to check wheather it contain value or nil it has nil then we will provide default value like this value ?? "default value"
 a ? : b- it is just like if else we can check all condition like ifelse in ternary condition
 
 */

/*
 UIViewController->UIResponder->NSObject->NSObjectProtocol
 UIButton,UITextField->UIControl->UIView->UIResponder->NSOBject->NSObjectProtocol
 UIImageView,UIScrollView,UILabel->UIView->UIResponder->NSObject->NSObjectProtocol
 UITableView,UITextView->UIScrollView->UIView->UIResponder->NSObject->NSObjectProtocol
*/

/*
 //https://blog.dunzo.com/swift-responder-chain-19a19fa0fadc
 Responder Chain:
 UIView->UIViewController->UIWindow->Appdelegate
 * Respoder Chain is the series of event it happned when we start to interact with view or application.
 * Like when we tap on UITextfield whole the seris of responder initiate, it is happend because  like UIViewControl, UIView,UIApplication subclass UIResponder
 *When we click on any view at that time UIKit is start to find the location of that touch and UIView traverse deepest level of hierarchy to find specifc touch. After finding the that view it assign first responder to it. after that UIKit create the object of UITouch and pass the same view to it.and UIKit calling the various api like touchesBegan etc.
 *
 *
 */

//UIControl: Control handle the action of buttons




//https://makeappicon.com
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//      application.registerForRemoteNotifications()
        registerForPushNotifications()

        return true
    }
    @objc func buttonActions(sender: UIButton){
        print(sender)
//        print(sender.next?.touchesBegan(.init(), with: .none) as Any)
    }
    
    
    
    func registerForPushNotifications(){

        /*
         Notification center handle all notification related activity in app.
         */
        /*
         requestAuthorization request authorization to show notification. Options indicate that which type of notification do you want
         */
        /*
         current: retrun the notification center object
         */
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound,.badge]) {
            [weak self] granted, error in

            print("Permission granted: \(granted)")
            guard granted else { return }
            self?.getNotificationSettings()
        }
    }

    func getNotificationSettings() {

        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications() // register device to iOS to APNS server and will get device token from APNS
            }
        }
    }
    
    


    // MARK: UISceneSession Lifecycle

//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
    
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        
            if let activity = options.userActivities.first, activity.activityType == "SecondScene" {
            return UISceneConfiguration(name: "Second Configuration", sessionRole: connectingSceneSession.role)
            }
            return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        
        let container = NSPersistentContainer(name: "SwiftDemo")
        
        
        // Migration Start
        //These line for core data versioning
        let description = NSPersistentStoreDescription()
        description.shouldMigrateStoreAutomatically = true
        description.shouldInferMappingModelAutomatically = true
        container.persistentStoreDescriptions = [description]
         // Migration End
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    
    
    // MARK: - Core Data Saving support
    func saveContext (){
        
        let context = persistentContainer.viewContext
        if context.hasChanges{
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

/*
 Push Notification Delegate
 */

extension AppDelegate: UNUserNotificationCenterDelegate {
        
    // This method will be called when app received push notifications in       
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
            UIApplication.shared.applicationIconBadgeNumber = 0
        print("userNotificationCenter")
        if #available(iOS 14.0, *) {
            completionHandler([.alert, .sound,.badge,.list,.banner])
        } else {
            // Fallback on earlier versions
            completionHandler([.alert, .sound,.badge])

        }
    }
    
    // User press on push notification
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        let aps = userInfo["aps"] as? [String: AnyObject]
        UIApplication.shared.applicationIconBadgeNumber = aps?["badge"] as! Int
       // (jsonDict["totalfup"] as! NSString).doubleValue
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
//        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let tokenParts = deviceToken.map{
            String(format: "%02.2hhx", $0)
        }

        let token = tokenParts.joined()
        print("Device Token: \(token)")
        
        let ary = ["a","b","c","d"]
        print(ary.joined())
    }
    
    
    // Silent Notification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
             let aps = userInfo["aps"] as? [String: AnyObject]
                print(aps as Any)

    }
      
    // openssl pkcs12 -in CertificatesDistributionDmy.p12 -out CertificatesDmy.pem -nodes -clcerts
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error){
        print("Failed to register: \(error)")
    }
}


/*
 App State
 
 Not Running State*
 - App has not been launched.
 - App was terminted by system.
 
 Inactive State*
 
 - App is on forground but not recieving any event.
 - App is an inactive state when recieving call and msg
 - App is an inactive state when phone has locked.
 
 
 Active State*
 - App is on forground and recieving event.
  
 Background State*
 
 - App is in the background and executing code(User manually put on background ).
 - App is in background when app is launching instead of inactive state
 
 Suspended State*
 - App is in the  background but not executing code.
 - Its is still on memory when it is in suspenderd state.
 - System put App is in the suspended state when low memory occur by doing this other forground app will get memory.
 */


/*
// git command
git checkout -b chandannewbranch: for create new branch
git checkout chandannewbranch : for switch the branch
 
 git init
 git add -A // --all  for all file -A for only changes file
 git commit -m 'test chandan'
 git push origin chandannewbranch // git push origin -u chandannewbranch
 git pull origin harinewbranch // dertination branch name

 */

//720
