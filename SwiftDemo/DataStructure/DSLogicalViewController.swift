//
//  DSLogicalViewController.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 21/03/22.
//

import UIKit

class CircularNode<T>{
    var data: T
    var next: CircularNode<T>?
    init(data: T) {
        self.data = data
    }
}


class CircularLoopLinkedList<T>{

    
   private var head: CircularNode<T>?
   private var tail: CircularNode<T>?
    
    
    func insertNode(data: T){
        
        let newNode = CircularNode(data: data)
        
        if let tempTail = tail{
            
            newNode.next = tempTail.next
            tempTail.next = newNode
            tail = newNode

        }else{
            head = newNode
            tail = newNode
            tail?.next = tail
        }
    }
    
    
    func checkCircularLoopOrNot()->Bool{
        
//        // first option
//        if tail!.next == nil{
//            return false
//        }else{
//            return true
//        }
        
        //second option
        
        var slow = head
        var fast = head
        
       /* while slow != fast {
            
            if fast == nil || fast?.next == nil{
                return false
            }
            slow = slow?.next
            fast = fast?.next?.next
        }
        */
        return true
        
    }
        
}


//https://practice.geeksforgeeks.org/explore?page=2&company[]=Paytm&sortBy=submissions
class DSLogicalViewController: UIViewController {

    override func viewDidLoad(){
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //        let array = [[0,0,0,1],[0,1,1,1],[1,1,1,1],[0,0,0,0],[0,1,1,1]]
        //        findRow(arry: array)
        
//        var stringValue = "azxxxzzy"
//        adjacentDuplicate(stringValue: &stringValue)
//        longestSubsequence()
//        missingNo()
//        palinDrome()
//          sorting()
//        print(swipe())
//        primeNo()
//        detectLooptInLinkedList()
        
        var editArray = [1,2,3]
        print(editArray)
        editArray.insert(10, at: 1)
        print(editArray)
        
        var arraySize = [Int?](repeating: nil, count:5)

//        arraySize.reserveCapacity(5)
        arraySize.append(1)
        arraySize.append(2)
        arraySize.append(3)
        arraySize.append(4)
        arraySize.append(5)
        print(arraySize)
        arraySize.append(6)
        print(arraySize)





        
//        let acb = -1
//        print(abs(acb))
//          sumZero()
    }
    
    
   // Detect Loop in linked list
    func detectLooptInLinkedList(){
        
        let circularLoopLinkedList = CircularLoopLinkedList<Int>()
        circularLoopLinkedList.insertNode(data: 10)
        circularLoopLinkedList.insertNode(data: 11)
        circularLoopLinkedList.insertNode(data: 12)
        
        if circularLoopLinkedList.checkCircularLoopOrNot(){
            print("Detect Loop in linked list")
        }else{
            print("Not Detect Loop in linked list")

        }
        
    }
    
    
    
    func sumZero(){
        let array = [4, 2, -3, 1, 6]
        
        var sumZero = 0
        var sumString = ""
        var isZero = false
        
        for i in 0..<array.count{
            sumZero = array[i]
            sumString.append("\(array[i]),")
            for j in i+1..<array.count{
                sumZero += array[j]
                sumString.append("\(array[j]),")
                if sumZero == 0{
                    isZero = true
                    break
                }
            }
            
            if isZero{
                break
            }
            sumZero = 0
            sumString = ""
        }
        
        print(sumString)
    }
    
    
    func primeNo(){
        
        
        var index = 0
        let num = 11
        
        for i in 1...num{
            
            if num % i == 0{
                print(i)
                index += 1
            }
        }
        
        if index == 2{
            print("prime no")
        }else{
            print("not prime no")
        }
    }
    
    
        
    func swipe(){
        var array = [2,1,4,5,3,6]

        for i in 0..<array.count/2{
            
            let temp = array[array.count-1 - i]
            array[array.count-1 - i] = array[i]
            array[i] = temp
        }
        
        print(array)
    }
    
    
    func sorting(){
        
        var array = [2,1,4,5,3,6]
        
        for i in 0..<array.count{
            
            for j in i+1..<array.count{
                
                if array[i] > array[j]{
                    
                    let temp = array[i]
                    
                    array[i] = array[j]
                    array[j] = temp
                }
            }
        }
        
        print(array)
    }
    
    func palinDrome(){
       let string = "aakaa"
       var isPalinDrome = true
        
        for (index, value) in string.enumerated(){
            
            
           let leftString = String(value)
            let rightIndex =  string.index(string.endIndex, offsetBy: (string.count - 1 - index) - string.count)
            let rightString = String(string[rightIndex])
            
            if leftString != rightString{
                isPalinDrome = false
                break
            }

            
            
            if (string.count-1)/2 == index{
                break
            }
            
                
        }
        
        
        print("string is  \(isPalinDrome)")

        
    }
    
    
    func missingNo(){
        
        let arr = [1, 2, 3, 4, 5, 6, 8, 9, 10]
        
        var sum = 0
        var total = 0
        
        let n = arr.count + 1 // where + 1 is missing count
        
        
        for value in arr{
            sum += value
        }
        
        total = n*(n+1) / 2
        
        print(total - sum)
        
        

    }
    
    func longestSubsequence(){
        
//        let array = [0,8,4,12,2,10,6,14,1,9,5,
//             13,3,11,7,15]
        let array = [10,9,2,5,3,7,101,18]
        
        //0 2 6 9 13 15, which has length 6

//        let array = [50, 3, 10, 7, 40, 80]
//        let array = [3, 2]
//        let array = [3, 10, 2, 1, 20]
//        let array = [5,8,3,7,9,1]

//        Output: 3
//        Explanation:Longest increasing subsequence
//        5 7 9, with length 3
        
        var subLength = 1
        var subTotal = 0
        var previousValue = 0
        var currentStringValue = ""
        var finalString = ""
        
        for i in 0..<array.count{
            previousValue =  array[i]
            currentStringValue.append("\(String(previousValue)), ")
            
            for j in i+1..<array.count{
                if previousValue < array[j]{
                    subLength += 1
                    previousValue = array[j]
                    currentStringValue.append("\(String(previousValue)), ")
                }
            }
            
            if subLength >= subTotal{
                subTotal = subLength
                finalString = currentStringValue
            }
            
            subLength = 1
            currentStringValue = ""

      }
        
        
        print(finalString)
    }
    
    
    
    func reverseWords(){
        
//        var stringValue = "geeks quiz practice code"
        var stringValue = "geeks"

        let stringArray = stringValue.components(separatedBy: " ")
        print(stringArray)
        
        for (index, value) in stringValue.enumerated(){
            
            let leftIndex = stringValue.index(stringValue.startIndex, offsetBy: index)
            let rightIndex = stringValue.index(stringValue.endIndex, offsetBy: (stringValue.count - 1 - index) - (stringValue.count))

            let temp = stringValue[leftIndex]
            stringValue.replaceSubrange(leftIndex...leftIndex, with: String(stringValue[rightIndex]))
            stringValue.replaceSubrange(rightIndex...rightIndex, with: String(temp))
            if (stringValue.count - 1) / 2 == index{
                break
            }
        }
        print(stringValue)
    }

    
    
    
    func reverseStr(){
            
            
            var stringValue = "xyzz"
            
            for (index, value) in stringValue.enumerated(){
                
                let leftIndex = stringValue.index(stringValue.startIndex, offsetBy: index)
                let rightIndex = stringValue.index(stringValue.endIndex, offsetBy: (stringValue.count-1 - index) -  stringValue.count)
                
                print(stringValue[leftIndex])
                print(stringValue[rightIndex])

                let leftTemp = value
                stringValue.replaceSubrange(leftIndex...leftIndex, with: String(stringValue[rightIndex]))
                stringValue.replaceSubrange(rightIndex...rightIndex, with: String(leftTemp))
                
                if stringValue.count/2 == index + 1{
                    break;
                }
            }
            
            print(stringValue)
    }
    
    
    func duplicateStr(){
            
            let stringValue = "zabcabx"
            print(stringValue)
        
        var dict: [String: Int] = [String: Int]()
            
            for (index, value) in stringValue.enumerated(){
                
                if !dict.keys.contains(String(value)){
                    dict[String(value)] = index
                }else{
                    print("duplicate \(value)")
                }
          }
        
        
        var nonDuplicateArray:[String] = []
        
        for value  in stringValue{
    
            if !nonDuplicateArray.contains(String(value)){
                nonDuplicateArray.append(String(value))
            }else{
                
                let removeIndex = nonDuplicateArray.firstIndex(of: String(value))
                nonDuplicateArray.remove(at: removeIndex!)
            }
        }
        print(nonDuplicateArray)
  }
    
    
         
    func adjacentDuplicate(stringValue: inout String){
         
        var pIndex: String.Index?
        var pIntIndex = 0
        
        for (index,value) in stringValue.enumerated(){
             
            if index == 0{
               pIndex = stringValue.index(stringValue.startIndex, offsetBy: index)
               pIntIndex = Int(index)
                continue
            }
            
             let currentIndex = stringValue.index(stringValue.startIndex, offsetBy: index)
//             print(str[currentIndex])
            
            if stringValue[pIndex!] != stringValue[currentIndex]{
                let diff =  Int(index) - pIntIndex
                if diff > 1{
                    
                    stringValue.replaceSubrange(pIndex!..<currentIndex, with: "")
                    defer {
                        adjacentDuplicate(stringValue: &stringValue)
                    }
                    print(stringValue)
                    break
                    
                }else{
                    pIndex = stringValue.index(stringValue.startIndex, offsetBy: index)
                    pIntIndex = Int(index)
                }
            }
         }
        
        print(stringValue)
     }
    
    
     
    
    var totalOnes = 0
    func findRow(arry:[Any]){
        
        var zeros = 0
        var ones = 0
        var index = 0
        
        for value in arry{
            
            if value is [Any]{
                findRow(arry: value as! [Any])
            }else{
                
                if value as! Int == 0{
                    zeros += 1
                }else{
                    ones += 1
                }
                
                if arry.count-1 == index{
                    if ones > zeros{
                        totalOnes += 1
                    }
                }
                    index += 1
            }
        }
        print(totalOnes)
    }
        
    
    func frequenciesCount(){
        
        let array = [2,2,3,3,2,5]
    
        var dict: [Int: Int] = [:]
        for value in array{
            let temp = (dict[value] ?? 0) + 1
            dict[value] = temp
        }
        
        for i in 1...5{
           
            if !dict.keys.contains(i){
                dict[i] = 0
            }
            
        }
        print(dict)
    }
    
    
    func zigzag(){
        //a<b>c<d>e<f
//        var array = [4,3,7,8,6,2,1]
        var array = [1,4,3,2]
        
        var i = 0
        var isEven = false
        while i < array.count - 1 {
                        
            if isEven{
                
                if array[i] < array[i + 1]{
                
                    let temp = array[i]
                    array[i] = array[i + 1]
                    array[i + 1] = temp
                }
                isEven = false
            }else{
                
                if array[i] > array[i + 1]{
                    let temp = array[i]
                    array[i] = array[i + 1]
                    array[i + 1] = temp
                }
                isEven = true

            }
            
            i += 1
         }
        print(array)
    }
    
    
    func sort0SAnd1S2S(){
        
//      var array = [0,1,2,0,1,2]
        var array = [0,1,0,1]
        
        var left = 0 //2
        var mid = 0  //3
        var right = array.count - 1

        var temp = 0
        
        while mid <= right {
            
            switch array[mid] {
            
                case 0:

                    temp = array[left]
                    array[left] = array[mid]
                    array[mid] = temp
                    left += 1
                    mid += 1
                    
                case 1:
                    
                    mid += 1
                    
                case 2:

                    temp = array[mid]
                    array[mid] = array[right]
                    array[right] = temp
                    right -= 1
             
                default:
                    print("default")
            }
        }
        print(array)
    }
    
    
}
