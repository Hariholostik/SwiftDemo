//
//  LinkedListController.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 23/02/22.
//

import UIKit

//=============== SingleLinkedList===============
//Created Node
class SNode<T>{
    var data: T
    var next: SNode<T>?
    init(data: T){
        self.data = data
    }
}

class SingleLinkedList<T>{
    
    var head: SNode<T>? // head is nil when list is empty
    var isEmpty: Bool{// check head is empty or not
        return head == nil
    }
    var getFirstNode: SNode<T>?{
        return head
    }
    
    var getLastNode: SNode<T>?{
        var tempHead = head
        while tempHead?.next != nil{
            tempHead = tempHead?.next
        }
        return  tempHead
    }
    
    var getMidNode: SNode<T>?{
        var slowNode = head
        var fastNode = head
        while fastNode?.next?.next != nil{
            slowNode = slowNode?.next
//            print("midNode: \(fastNode?.next?.next?.data)")
            fastNode = fastNode?.next?.next
        }
        return slowNode
    }

    
    
    func setAtLastNode(data: T){
        
        let newNode = SNode(data: data)
        var tempHead = head
        
        while tempHead?.next != nil {
            tempHead = tempHead?.next
        }
        tempHead?.next = newNode
    }
    
    
    
    // add new node
    func insertNode(data: T){
        let newNode = SNode(data: data)
        
        if var tempHead = head{
            
            while tempHead.next != nil{
                tempHead = tempHead.next!
            }
            tempHead.next = newNode // added new node at end of the list
            
        }else{
            head = newNode // added new node at end of the list
        }
    }
    
    
    // insert node any position
    func insertNodeAtPosition(data: T, position: Int){
        let newNode = SNode(data: data)
        
        if position == 0 || head == nil{
            newNode.next = head
            head = newNode
        }else{
            
            var tempHead = head
            
            for _ in 0..<position - 1{
                tempHead = tempHead?.next
            }
            newNode.next = tempHead?.next// step1
            tempHead?.next = newNode// step2
        }
    }
    
    
    
    // insert node at mid position
    func insertAtMid(data: T){
        
        let newNode = SNode(data: data)

        var slowPrevious = head
        var slow = head
        var fast = head
        
        while fast?.next?.next != nil {
            slowPrevious = slow
            slow = slow?.next
            fast = fast?.next?.next
        }
        newNode.next = slow?.next
        slow?.next = newNode
//        newNode.next = slow
//        slowPrevious?.next = newNode
    }

    
    
    // delete node any position
    func deleteAtPosition(position: Int){
        
        if head == nil{
            return
        }
        
        var headTemp = head
        
        if position == 0{
            head = headTemp?.next // change the head
        }

        for _ in 0..<position-2 where headTemp != nil{
            headTemp = headTemp?.next
        }
        
        let tempNodeAfterDeleted = headTemp?.next?.next
        headTemp?.next = tempNodeAfterDeleted
    }
    
    
    func deleteLast(){
        
        var tempHead = head
//        var previousTemp = head
        while tempHead?.next?.next != nil{
//            previousTemp = tempHead
            tempHead = tempHead?.next
        }
//        previousTemp?.next = nil
        tempHead?.next = nil
    }
    
    
    func deleteMid(){
        
        var slowPrevious = head
        var slow = head
        var fast = head
        
        while fast?.next?.next != nil {
            
            slowPrevious = slow
            slow = slow?.next
            fast = fast?.next?.next
        }
        
        slowPrevious?.next = slow?.next
    }
    
    
    func printList(){
        var current = head
        while current != nil{
            print("added value: \(current!.data as! Int )")
            current = current?.next
        }
    }
    
    func reversePrintList(node: SNode<T>?){
        
        if node == nil{
           return
        }
        reversePrintList(node: node?.next)
        print(node?.data as Any)
    }
    
    func getReverseNode(){
        reversePrintList(node: head)
    }
    
    
    func reverseLinkedList(){
        
        let length = getLinkedListLength()
        var leftHead = head

        
        for i in 0..<length/2{
            
            let rightHead = getLeftRightNode(rightIndex: length - i)
            
            let tempLeftData = leftHead?.data
            leftHead?.data = rightHead!.data
            rightHead?.data = tempLeftData!
            
            leftHead = leftHead?.next

        }
        
    }

    
    func getLinkedListLength() -> Int{
        var index = 0
        var headTemp = head
        while  headTemp != nil {
            headTemp = headTemp?.next
            index += 1
        }
        return index
    }
    
    
    func getLeftRightNode(rightIndex: Int) -> (SNode<T>?){
        
        var index = 1
        
        var rightHead = head
        while rightHead != nil {
            if index == rightIndex{
                return rightHead
            }
            rightHead = rightHead?.next
            index += 1
        }
        return rightHead
    }
}






//=============== DoubleLinkedList===============

class DNode<T>{
    var data: T
    var next: DNode<T>?
   /*weak*/  var previous: DNode<T>?
    init(data: T) {
        self.data = data
    }
}


class DoubleLinkedList<T>{
    var head: DNode<T>?
    var tail: DNode<T>?
    
    func insertNode(data: T){
        
        let newNode = DNode(data: data)
        if let tempTail = tail{
            newNode.previous = tempTail
            tempTail.next = newNode
        }else{
            head = newNode
        }
        
        tail = newNode
  }
    
    
    
    func insertNodeAtPosition(data: T, position: Int){
        
        let newNode = DNode(data: data)
        
        if position == 0, tail == nil{
            head = newNode
            tail = newNode
        }else{

             var index = position
             var headTemp = head
            
            while headTemp != nil {
                
                if index == 0{
                    break
                }
                index -= 1
                headTemp = headTemp?.next
            }
            
            if headTemp ==  nil{
                return
            }
            
            
            if headTemp?.previous == nil{
                head = newNode
            }
            
            newNode.previous = headTemp?.previous
            headTemp?.previous?.next = newNode
            newNode.next = headTemp
            headTemp?.previous = newNode
        }
    }
       
    func deleteAtPosition(position: Int){
        
        if head == nil || tail?.previous == nil{
            head = nil;tail = nil
            return
        }else if position == 0{
            
            var headTemp = head
            headTemp?.next?.previous = headTemp?.previous
            headTemp?.previous?.next = headTemp?.next
            head = headTemp?.previous?.next
        }
        
        var headTemp = head
        var index = position

        while headTemp != nil{
            if index == 0{
                break
            }
            headTemp = headTemp?.next
            index -= 1
        }
        
        if headTemp == nil{
            print("index out of bound")
            return
        }
        headTemp?.next?.previous = headTemp?.previous
        headTemp?.previous?.next = headTemp?.next
 }
    
    func printDoubleList(){
        var tempHead = head
        while tempHead != nil {
            print(tempHead?.data as Any)
            tempHead = tempHead?.next
        }
    }
}



//=============== CircularLinkedList===============

class CNode<T>{
    var data: T
    var next: CNode<T>?
    init(data: T) {
        self.data = data
    }
}


class CircularLinkedList<T>{

    var head: CNode<T>?
    var last: CNode<T>?
    
    func insertNode(data: T){
        
        let newNode = CNode(data: data)
        
        if let tempLast = last{
            
            newNode.next = tempLast.next
            tempLast.next = newNode
            last = newNode
                        
        }else{
            head = newNode
            last = newNode
            last?.next = last
        }
    }
    
    
    func addAtBegin(data: T){
        let newNode = CNode(data: data)
        
        if last == nil{
            
            head = newNode
            last = newNode
            last?.next = last
            return
        }
        
        newNode.next = head
        last?.next = newNode
        head = newNode
    
    }
    
    func addAtEnd(data: T){
        
        let newNode = CNode(data: data)
        
        if last == nil{
            
            head = newNode
            last = newNode
            last?.next = last
            return
        }
        
        newNode.next = last?.next
        last?.next = newNode
        last = newNode
    
    }
    
    
    func deleteLast(){
        
        var lastPreviousTemp = last
        var headTemp = head

        while !(headTemp === last) {
            lastPreviousTemp = headTemp
            headTemp = headTemp?.next
        }
        lastPreviousTemp?.next = last?.next
        last = lastPreviousTemp
    }
    
    
    func deleteFirst(){
        let headTemp = head
        last?.next = headTemp?.next
        head = headTemp?.next
    }
    
    
    func printCircularList(){
        
        var tempHead = head
        print(tempHead?.data as Any)
        tempHead = tempHead?.next
        while !(tempHead ===  head) {
            print(tempHead?.data as Any)
            tempHead = tempHead?.next
        }
    }
}



//https://www.journaldev.com/20995/swift-linked-list
//https://www.geeksforgeeks.org/circular-linked-list/
class LinkedListController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        singleLinkedList()
//        doubleLinkedList()
//        recursionStack(n: 1)
//         circularLinkedList()
           fabinnocoSeries()
    }
    
    
        
    func circularLinkedList(){
        
        let circularLinkedList = CircularLinkedList<Int>()
        circularLinkedList.insertNode(data: 5)
        circularLinkedList.insertNode(data: 6)
        circularLinkedList.insertNode(data: 7)

        circularLinkedList.printCircularList()
        circularLinkedList.addAtBegin(data: 4)
        circularLinkedList.printCircularList()
        circularLinkedList.addAtEnd(data: 8)
        circularLinkedList.printCircularList()
        circularLinkedList.deleteLast()
        circularLinkedList.printCircularList()
        circularLinkedList.deleteFirst()
        circularLinkedList.printCircularList()
        
    }
    
    
    func doubleLinkedList(){
        let doubleLinkedList = DoubleLinkedList<Int>()
        doubleLinkedList.insertNode(data: 5)
        doubleLinkedList.insertNode(data: 6)
        doubleLinkedList.insertNode(data: 7)
        doubleLinkedList.printDoubleList()
        print("tail:\(doubleLinkedList.tail?.data)")

        print("\n")
        
        doubleLinkedList.insertNodeAtPosition(data: 8, position: 0)
        doubleLinkedList.insertNodeAtPosition(data: 11, position: 3)

        doubleLinkedList.printDoubleList()
        print("tail:\(doubleLinkedList.tail?.data)")
        doubleLinkedList.insertNode(data: 17)
        doubleLinkedList.printDoubleList()
        print("tail:\(doubleLinkedList.tail?.data)")
        doubleLinkedList.deleteAtPosition(position: 2)
        doubleLinkedList.printDoubleList()

    }
    
    func singleLinkedList(){
        
        let linkedListObj = SingleLinkedList<Int>()
//        linkedListObj.insertNodeAtPosition(data: 15, position: 3)
//        print(linkedListObj.printList())
        
//        linkedListObj.insertNode(data: 5)
//        linkedListObj.insertNode(data: 6)
//        linkedListObj.insertNode(data: 7)
//        linkedListObj.insertNode(data: 10)

        
//        linkedListObj.insertNodeAtPosition(data: 9, position: 3)
//        linkedListObj.insertNodeAtPosition(data: 19, position: 5)
//        linkedListObj.insertNodeAtPosition(data: 21, position: 5)
//        print(linkedListObj.printList())
        
//        linkedListObj.deleteAtPosition(position: 3)
//        print(linkedListObj.printList())
        
//        print(linkedListObj.getFirstNode?.data as Any)
//        print(linkedListObj.getLastNode?.data as Any)
//        print(linkedListObj.getMidNode?.data as Any)
//        linkedListObj.setAtLastNode(data: 51)
//        print(linkedListObj.getLastNode?.data as Any)
//        print(linkedListObj.printList())
//        linkedListObj.deleteLast()
//        print(linkedListObj.printList())
//        linkedListObj.deleteMid()
//        print(linkedListObj.printList())
        
//        print(linkedListObj.getMidNode?.data as Any)
//        linkedListObj.insertAtMid(data: 100)
//        print(linkedListObj.printList())
//        print(linkedListObj.getMidNode?.data as Any)
//        linkedListObj.insertAtMid(data: 150)
//        print(linkedListObj.printList())
//        print("Printing Reverse:")
//        linkedListObj.getReverseNode()
//        print("length: \(linkedListObj.getLinkedListLength())")
        
        // reverse the linkedList
        
        singleLinkedListReversed(linkedListObj: linkedListObj)

        
    }
    
    
    func singleLinkedListReversed( linkedListObj: SingleLinkedList<Int>){
        
        linkedListObj.insertNode(data: 1)
        linkedListObj.insertNode(data: 2)
        linkedListObj.insertNode(data: 3)
        linkedListObj.insertNode(data: 4)
        linkedListObj.insertNode(data: 5)
        print("not reversed")
        linkedListObj.printList()
        linkedListObj.reverseLinkedList()
        print("reverse reversed")
        linkedListObj.printList()
    }
    
    
    func recursionStack(n: Int){
        
        if n == 10{
            return
        }
        recursionStack(n: n + 1)
        print(n)
        
    }
        
    func fabinnocoSeries(){
        
        var f1 = 0
        var f2 = 1
        var sum = 0
            sum = f1 + f2
            print(f1)
            print(sum)
        
        for _ in 2..<10{
            print(sum)
            f1 = f2
            f2 = sum
            sum = f1 + f2
        }
        
    }
}
//Fibonacci Series 10= 0, 1, 1, 2, 3, 5, 8, 13, 21, 34,
