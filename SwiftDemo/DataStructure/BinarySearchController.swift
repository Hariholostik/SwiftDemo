//
//  BinarySearchController.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 4/03/22.
//

import UIKit

/*
Rules
 1.All nodes in the left subtree must have lesser value than the value in the root node.
 2.All node in the right subtree must have greater value than the value in the root node.
 3.Left and Right subtrees of root node should follow the above rules recursively.
 */

class Node<T>{
    var data: T
    var leftNode: Node?
    var rightNode: Node?
    init(leftNode: Node? = nil, rightNode: Node? = nil, data: T) {
        self.leftNode = leftNode
        self.rightNode = rightNode
        self.data = data
    }
    
}


class BTS<T>: Comparable & CustomStringConvertible{
    static func < (lhs: BTS<T>, rhs: BTS<T>) -> Bool {
       return rhs > lhs
    }
    var description: String = ""
    static func == (lhs: BTS<T>, rhs: BTS<T>) -> Bool {
        return lhs === rhs
    }
    
    
    var rootNode: Node<T>?
    
    func addNode(data: T){
        
        let node = Node(data: data)
        
        if let tempRootNode = self.rootNode{
            
            
        }else{
            self.rootNode = node
        }
    }
    
    func insert(currentRoot: Node<T>, newNode: Node<T>){
//        if currentRoot.data < newNode.data{
//
//        }else{
//
//        }
    }
}


//https://www.journaldev.com/21454/swift-binary-search-tree
class BinarySearchController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        frequenciesCount()
//        weakDelegate()
//        let view = senderVC
    }
    
    
    func retainCycle(){
        
        var employee: Employee? = Employee()
        var compnay: Company? = Company()
        
        employee?.com = compnay
        compnay?.emp = employee
//        employee = nil
//        compnay?.emp = nil
//        compnay = nil
//        employee?.com = nil
    }

    
    func weakDelegate(){
        
//        let vc = SenderVC()
//            vc.delegate = self
        let recieverVC = RecieverVC()
        recieverVC.callSender()
//        let view =  recieverVC.senderVC
//        print(view)
    }
    
    
//
    lazy var senderVC: SenderVC = {
        let vc = SenderVC()
        vc.delegate = self
        return vc
    }()

    deinit {
        print("RecieverVC deinit")
    }
//
    
}


// why delegate is weak?
protocol SendDataDelegate: AnyObject{}

class SenderVC: UIView{
     var delegate: SendDataDelegate?
    deinit {
        print("SenderVC deinit")
    }
}

class RecieverVC: UIView{
    lazy var senderVC: SenderVC = {
        let vc = SenderVC()
        vc.delegate = self
        return vc
    }()
    
    func callSender(){
        
        let view = senderVC
//        print(view)
//        var senderVC: SenderVC = SenderVC()
//         senderVC.delegate = self
//        self.addSubview(senderVC)
    }
    
    
    deinit {
        print("RecieverVC deinit")
    }
}


extension RecieverVC: SendDataDelegate{
}

extension BinarySearchController: SendDataDelegate{
}


class Employee{
   weak var com: Company?
    init(){}
    deinit {
        print("Employee deinit")
    }
}


class Company{
    
    var emp: Employee?
    init(){}
    deinit {
        print("Company deinit")
    }
}


class Coder: UIView{
  
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
