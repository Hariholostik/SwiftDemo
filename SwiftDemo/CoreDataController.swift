//
//  CoreDataController.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 13/02/22.
//

import UIKit
import CoreData
import AVFoundation // AVPlayer, AVPlayerLayer, Can create Custom Player.
import AVKit // already have AVFoundation, AVPictureInPictureController,AVPlayerViewController.
import UIKit.UIColor //add this

protocol Emp {
    func f1()
    func f2()
}

protocol Org: Emp{
   func f1()
}

extension Org {
    func f1(){
        print("Org")
    }
}



extension CoreDataController: Org{
    func f2() {
        print("f2")
    }
    
    func f1() {
        print("f1")
    }
    
//    func f1() {
//        print("f1")
//
//    }
}


let cache = NSCache<AnyObject, AnyObject>()

struct S {}


/*
 Transformable: This data type is used when you want to save custom data type like UIColor
 */
//Core Data Versioning
//https://medium.com/@maddy.lucky4u/swift-4-core-data-part-5-core-data-migration-3fc32483a5f2
/*
 If we change the any thing(add new entity,attribute, optional, noptional) in live project app we need to migrated the coredata.
 stepe1: Select the .xccoredatamodelId
 steps2: Then select the editor and click on "Add Model Version" then new .xcdatamodelId is created
 step3: Click on new create .xcdatamodelId and choose  Model Version from right menu and select new created one.
 steps4: And goto appdelegate and write some code on NSPersistentStoreDescription and enable the automatically migration true and  automatically model mapping true
 
 */


class CoreDataController: UIViewController {
    
    var manageObjectContext: NSManagedObjectContext!
    
    @IBOutlet weak var yCenter: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        getManagedObjectContextFromAppDelegate()
//        readData()
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        print(path[0])
        
        createOneToManyData()
//        updateOneToManyData()
        createData()
//        readData()
        
//        localization()
//        avPlayer()
        
//        imageCache(imgUrl: "key")
//        imageCache(imgUrl: "key")
        
//        view.layoutIfNeeded()
//        view.setNeedsLayout()
//        view.setNeedsDisplay()
        
//        GCDLikeNSOperationQueue()
        
        
        
    //Copy on Write - COW - if the collection variable has same element, both the variable pointed to same address
        
        let s1 = S()
        let s2 = s1
        
//        if s1 == s2{ // occur the error
//            print(s2)
//        }

        let str1 = "str"
        var str2 = "str"
        
        if str1 == str2{
            print(str2)
        }
        
        str2.append("6")
        
        if str1 == str2{
            print(str2)
        }
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
//        UIView.animate(withDuration: 1.0, delay: 5.0, options: .curveEaseIn) {
//            self.yCenter.constant = self.yCenter.constant == 100 ? -100 : 100
//                self.view.layoutIfNeeded()
//        } completion: { (bool) in
//
//        }
        
        UIView.animate(withDuration: 1.0) {
            self.yCenter.constant = self.yCenter.constant == 100 ? -100 : 100
            self.view.layoutIfNeeded()
//            self.view.setNeedsLayout()

        }
    
    }
        
    
    
    func imageCache(imgUrl: String){
        
        
        if let cacheImg = cache.object(forKey: imgUrl as AnyObject) as? UIImage {
            print(cacheImg)
        }else{
            let img = #imageLiteral(resourceName: "soso")
            cache.setObject(img, forKey: imgUrl as AnyObject)
        }
    }
    

    // Technique #1  import UIKit
    //https://nshipster.com/image-resizing/

    func resizedImage(at url: URL, for size: CGSize) -> UIImage? {
        guard let image = UIImage(contentsOfFile: url.path) else {
            return nil
        }

        let renderer = UIGraphicsImageRenderer(size: size)
        
        let resizeImage =  renderer.image { (context) in
            image.draw(in: CGRect(origin: .zero, size: size))
        }
        
        return resizeImage
    }
    
    
    func test(url: URL, size: CGSize)->UIImage?{
        guard let img = UIImage(contentsOfFile: url.path) else {
            return nil
        }
        
        let render = UIGraphicsImageRenderer(size: size)
        
        let image = render.image { (context) in
            img.draw(in: CGRect(origin: .zero, size: size))
        }
        return image
    }
    
    func localization(){
        
        //view.semanticContentAttribute = .forceRightToLeft //for urdu or other RTL supporter
        //https://lokalise.com/blog/getting-started-with-ios-localization/
        //https://medium.com/swlh/app-localization-in-swift-ios-swift-guide-baa2c2e4298e
        let welcome = NSLocalizedString("welcome_screen_title", comment: "")
        let login = NSLocalizedString("login_button", comment: "")

        print(welcome)
        print(login)
    }
    
    
    func avPlayer(){
        
//        let localUrl = Bundle.main.path(forResource: "name", ofType: "mp4")
//        let url = URL(string:"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4")
        let url = NSURL(string: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4")

        let avPlayer = AVPlayer(url: url! as URL)
        let avPlayerLayer = AVPlayerLayer(player: avPlayer)
        avPlayerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(avPlayerLayer)
        avPlayer.play()
    }
        
    
func getCoreDataDBPath() {
        let path = FileManager
            .default
            .urls(for: .applicationSupportDirectory, in: .userDomainMask)
            .last?
            .absoluteString
            .replacingOccurrences(of: "file://", with: "")
            .removingPercentEncoding

        print("Core Data DB Path :: \(path ?? "Not found")")
    }
    
    
    
    func dummyF1(name: String? = nil){
        
        print(name as Any)
    }
    
    func getManagedObjectContextFromAppDelegate(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        manageObjectContext = appDelegate.persistentContainer.viewContext
    }
    
    
    // one to one relationship
    
    func createData(){
        // getting entity from manageobjectmodel
                        
        let customerEntity = NSEntityDescription.entity(forEntityName: "CustomerList", in: manageObjectContext)
        let orderEntity = NSEntityDescription.entity(forEntityName: "CustomerOrder", in: manageObjectContext)

        let customerManageObject = NSManagedObject(entity: customerEntity!, insertInto: manageObjectContext)
        let orderManageObject = NSManagedObject(entity: orderEntity!, insertInto: manageObjectContext)
        
//        customerEntity?.properties = []
        
        let customerUuid = UUID()
        let orderUuid = UUID()
            
        let profileImage = #imageLiteral(resourceName: "soso").pngData()
        
        let colorName: UIColor = .red
        
        customerManageObject.setValue(customerUuid, forKey: "userId")
        customerManageObject.setValue("6666666666", forKey: "phone")
        customerManageObject.setValue("pira", forKey: "name")
        customerManageObject.setValue("pira@gmail.com", forKey: "email")
        customerManageObject.setValue(profileImage, forKey: "profileImage")
        customerManageObject.setValue(orderManageObject, forKey: "toOrder")
        customerManageObject.setValue(colorName, forKey: "colorName")
            
        orderManageObject.setValue(orderUuid, forKey: "userId")
        orderManageObject.setValue("smart watch", forKey: "productName")
        orderManageObject.setValue("900", forKey: "price")
        orderManageObject.setValue(customerManageObject, forKey: "toCustomer")
        
        //jupitorMoney
        
         let appdalegate = UIApplication.shared.delegate as! AppDelegate
                
        let mainQueue = NSManagedObjectContext(concurrencyType:.mainQueueConcurrencyType)
        let concurrenctQueue = NSManagedObjectContext(concurrencyType:.privateQueueConcurrencyType)

        
        let backgroundQueue = appdalegate.persistentContainer.newBackgroundContext()
    
           backgroundQueue.performAndWait { [weak self]  in
//       appdalegate.persistentContainer.performBackgroundTask {[weak self]  (context) in// background
            
            do{
                print("background")
                try self?.manageObjectContext.save()
            }catch let error{
                    print(error)
            }
            
            if Thread.isMainThread{
                print("performBackgroundTask main")
            }else{
                print("performBackgroundTask not main")
            }
            self?.readData()
        }
        
        
    }
    
    
    func readData(){
        
                
        //%@ <= date AND date <= %@
        let readFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CustomerList")
//        readFetch.predicate = NSPredicate(format: "%@ <= price AND price >=%@", "500", "900")
//        readFetch.predicate = NSPredicate(format: "%@ >= price AND %@ <= price", argumentArray: ["500", "900"])
     
        manageObjectContext.performAndWait { // sync main queue
            if Thread.isMainThread{
                print("performAndWait main")
            }else{
                print("performAndWait not main")
            }
        }
        
        
        manageObjectContext.perform { // async main queue
            if Thread.isMainThread{
                print("perform main")
            }else{
                print("perform not main")
            }
        }
                
        
        do{
            let customerList = try manageObjectContext.fetch(readFetch)
            for customer in customerList as! [NSManagedObject]{
                print(customer.value(forKey: "colorName")!)
          }
        }catch let error{
            print(error)
        }
    
    }
    
    
    
    //8065787654
    func updateData(searchValue: String, updateValue: String){
        
        let updateFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CustomerList")
        updateFetch.predicate = NSPredicate(format: "phone = %@", searchValue)
        let customerList = try? manageObjectContext.fetch(updateFetch)
        
        let valueSet: [NSManagedObject] =  customerList as! [NSManagedObject]
            
            let updateData = valueSet[0]
            updateData.setValue(updateData.value(forKey: "userId"), forKey: "userId")
            updateData.setValue(updateValue, forKey: "phone")
            updateData.setValue(updateData.value(forKey: "name"), forKey: "name")
            updateData.setValue(updateData.value(forKey: "email"), forKey: "email")
            updateData.setValue(updateData.value(forKey: "profileImage"), forKey: "profileImage")
            updateData.setValue(updateData.value(forKey: "toOrder"), forKey: "toOrder")
        
        do{
           try manageObjectContext.save()
        }catch{
            print(error)
        }
    }
    
    
    
    func deleteData(serachValue: String){
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "CustomerList")
        deleteFetch.predicate = NSPredicate(format: "name = %@", serachValue)
        
        let deleteList = try? manageObjectContext.fetch(deleteFetch)
        
        let deleteData: [NSManagedObject] = deleteList as! [NSManagedObject]
        
        let delete = deleteData[0]
        
        manageObjectContext.delete(delete)

        do{
           try manageObjectContext.save()
        }catch{
            print(error)
        }
   }
    
    
    
    // one to many relationship
    func createOneToManyData(){
        
        let sureassureUserEntity = NSEntityDescription.entity(forEntityName: "SureassureUser", in: manageObjectContext)
        let scanHistoryEntity = NSEntityDescription.entity(forEntityName: "ScanHistory", in: manageObjectContext)
        
        let sureassureData = NSManagedObject(entity: sureassureUserEntity!, insertInto: manageObjectContext)
        sureassureData.setValue("102", forKey: "userId")
        sureassureData.setValue("Hari Giri", forKey: "name")
        
        let scanHistoryData1 = NSManagedObject(entity: scanHistoryEntity!, insertInto: manageObjectContext)
        scanHistoryData1.setValue("Android", forKey: "productName")
        scanHistoryData1.setValue(" MI 6, 2gb ram", forKey: "productDetails")
        scanHistoryData1.setValue(Date(), forKey: "scanDate")
        scanHistoryData1.setValue("203", forKey: "scanId")
        scanHistoryData1.setValue(sureassureData, forKey: "toSureassureUser")

        
        let scanHistoryData2 = NSManagedObject(entity: scanHistoryEntity!, insertInto: manageObjectContext)
        scanHistoryData2.setValue("Android", forKey: "productName")
        scanHistoryData2.setValue(" Samsung 12, 4gb ram", forKey: "productDetails")
        scanHistoryData2.setValue(Date(), forKey: "scanDate")
        scanHistoryData2.setValue("204", forKey: "scanId")
        scanHistoryData2.setValue(sureassureData, forKey: "toSureassureUser")

        let scanData: Set<NSManagedObject> = [scanHistoryData1,scanHistoryData2]

        sureassureData.setValue(scanData, forKey: "toScanHistory")
        
        do{
           try manageObjectContext.save()
        }catch{
            print(error)
        }
        
    }
    
    
    func updateOneToManyData(){
        
        let updateRecordsFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "SureassureUser")
        updateRecordsFetch.predicate = NSPredicate(format: "userId = %@", "101")
        
        let allData: [NSManagedObject] = try! manageObjectContext.fetch(updateRecordsFetch) as! [NSManagedObject]
        let nsmanegeObj = allData[0]
        
        let toScanHistory = nsmanegeObj.value(forKey: "toScanHistory")
        
        
        for value in toScanHistory as! Set<NSManagedObject>{
            print(value)
//            toScanHistory
        }
        
//        let scanHistoryEntity = NSEntityDescription.entity(forEntityName: "ScanHistory", in: manageObjectContext)
//        let scanHistoryData3 = NSManagedObject(entity: scanHistoryEntity!, insertInto: manageObjectContext)
//        scanHistoryData3.setValue("Laptop", forKey: "productName")
//        scanHistoryData3.setValue("HP 16gb ram", forKey: "productDetails")
//        scanHistoryData3.setValue(Date(), forKey: "scanDate")
//        scanHistoryData3.setValue("205", forKey: "scanId")
//        scanHistoryData3.setValue(nsmanegeObj, forKey: "toSureassureUser")
//
//        let scanData: Set<NSManagedObject> = [scanHistoryData3]
//        nsmanegeObj.setValue(scanData, forKey: "toScanHistory")
        
        do{
           try manageObjectContext.save()
        }catch{
            print(error)
        }
    }
    
    
    func GCDLikeNSOperationQueue(){
        
        
//        DispatchQueue.main.async {
//            print("Task 6 Done  \(Thread.current)")
//        }
//
      /*  let sQueue = DispatchQueue(label: "com.swiftpal.dispatch.serial")
        
        sQueue.async() {
            Thread.sleep(forTimeInterval: 5) // Wait for 3 seconds
            print("Task 1 Done \(Thread.current)")
        }

        sQueue.async(){
//            Thread.sleep(forTimeInterval: 1) // Wait for 1 second.
            print("Task 2 Done \(Thread.current)")
        }
        */
        
              
        let cQueue = DispatchQueue(label: "com.cun", qos: .default, attributes: .concurrent)
        cQueue.sync() {
            Thread.sleep(forTimeInterval: 10) // Wait for 3 seconds
            print("Task 3 Done \(Thread.current)")
        }

        cQueue.async(){
//            Thread.sleep(forTimeInterval: 1) // Wait for 1 second.
            print("Task 4 Done \(Thread.current)")
        }
        
        return
        
        let list = [Int](1...5)
        let arrayOfDictionaries = list.enumerated().map { (a, b) in
        return [a : b]
        }
        
        
        let aryOfDict = list.enumerated().map { (x,y) -> [Int: Int] in
            return [x:y]
        }
        
        print(aryOfDict)

      let ary  = list.map { (a) in
            return a + 1
        }
        
        print(ary)
        
        print(arrayOfDictionaries)

//        [{0: 1},{1: 2},…]
        
        
        
        let queue = DispatchQueue.global()

        
//Cuncurrent
        queue.sync {
            print("cuncurrent: async")
            queue.sync {
                print("cuncurrent: sync")
            }
        }
        
        
        queue.async {
            print("cuncurrent: async")
            queue.sync {
                print("cuncurrent: sync")
            }
        }
                
        
//Serial

        DispatchQueue.main.async {
            print("serial: async")
            DispatchQueue.main.sync {//crash
                print("serial: sync")
            }
        }
        
        DispatchQueue.main.sync {//crash
            print("serial: async")
            DispatchQueue.main.async {
                print("serial: sync")
            }
        }
        
        
        return
        
        queue.resume()
        queue.suspend()
        
        
        let workItem = DispatchWorkItem{
            print("Do Something")
        }
        
        queue.async(execute: workItem)
        workItem.cancel()
        
        queue.async(flags: .barrier) {
            print("FirstOperation")
        }
        
        queue.async{
            print("SecondOperation")
        }
        
        queue.async(flags: .barrier) {
            print("ThirdOperation")

        }
        
        if workItem.isCancelled{
            print("isCancelled")
        }
    }
}
