//
//  ARCMemoryController.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 8/11/21.
//

//https://stevenpcurtis.medium.com/swift-self-weak-or-unowned-7e2327974f36
//https://dev-wd.github.io/swift/arcmrc/
//https://www.youtube.com/watch?v=jdlRzJEXuc4

import UIKit
//https://learnappmaking.com/weak-vs-strong-references-swift/#whats-a-strong-reference
/*
 To check memory leak we will use instruments tool
 Debug memory leak: go to edit schema-> diagnostics-> Malloc Stack Logging-> Live allocation only.
 Then Click on Debug Memory Graph(icon like share) on bottom of Xcode
*/

//ARC: Swift uses Automatic Reference Counting (ARC) to track and manage your app’s memory usage. In most cases, this means that memory management “just works” in Swift, and you don’t need to think about memory management yourself. ARC automatically frees up the memory used by class instances when those instances are no longer needed.



//ARC is a compile time feature. The compiler inserts the necessary retain/release calls at compile time, but those calls are executed at runtime, just like any other code.


//ARC actually helps store references into memory and helps clean up when it is not being used.

//https://blog.devgenius.io/memory-management-part-2-reference-cycles-closures-and-debugging-59b917dc064b
// Retain Cycle/ Strong Referance Cycle: When two instance of a class hold(or pointing) strong reference to each other then it will create retain cycle.

//ClosureRetain Cycle: However, if a lazy variable is a closure, it would need to capture self weakly/unowned otherwise it will create the strong reference


/*
//Referance Counting/Retain Count: When one instance  hold by multiple referance variable its means that the referance counting is same as number of variable hold that referances.
 eg.
 let person1 = Person("Cavin")// referanceCount = 1, memoryAddress = 1010110
 let person2 = person // referanceCount = 2, memoryAddress = 1010110
 let person3 = person1 // referanceCount = 3, memoryAddress = 1010110
 
 person1 = nil // referanceCount = 2, memoryAddress = 1010110
 person2 = nil // referanceCount = 1, memoryAddress = 1010110
 person3 = nil // referanceCount = 0, memoryAddress = 1010110
 */

/*
 https://stackoverflow.com/questions/9859719/objective-c-declared-property-attributes-nonatomic-copy-strong-weak
 https://stackoverflow.com/questions/8927727/objective-c-arc-strong-vs-retain-and-weak-vs-assign
 https://medium.com/bitmountn/attributes-of-property-nonatomic-retain-strong-weak-etc-b7ea93a0f772
 https://coderedirect.com/questions/52853/objective-c-declared-property-attributes-nonatomic-copy-strong-weak
 There are three types of attributes in ARC:-
 
 **The main difference between weak and unowned is that weak is optional while unowned is non-optional.
 /*
 //https://www.youtube.com/watch?v=Lzl9h_MovJg
 weak vs unowned = weak is optional, unowned is non optional, like if we have label weak and we do nil and when we use that label with optional it will not crash but if your label is unowned then we do nil then we use that label it will crash the app
 
 weak label, label = nil, label?.text = "not crash" - it has ?
 unowned label, label = nil, label.text = "crash" - it has not ?
 strong label, label = nil, label?.text = "not crash" - it has ?
 
 Weak And Unowned=  Like we have A class and B Class, In B class we are calling API after api success we will update the Label. When we navigate back from B class at same B class deallocated from memory as well as same time the label in closure(api call) also deallocated it will not wait the api success to deallocate the Label.
 
 Strong= It Support the Retain couting technique that why The Label retain count is 2(one is created time and other in closure) so Label will not deallocated from memory until api give success.


 */
 
 
 
 1.Accress Attributes: readwrite, readonly
 2.Threading Attributes: atomic,nonatomic
 3.Memory Managment Attributes: strong,weak,unowned,copy,assign,retain,getter,setter,unsafe_unretained
 
 weak/ optional /referance type/only var: weak referance will not 'own' the object, weak referance will not 'alive' any object because it is not support referance couting techniue, it will deallocate when it is not in used.   ,we can not use weak with primitive data types because primitive data types is value types, its only use with class and class-bound protocol types.
 
 strong: default, strong referenace is use when we want to 'own' the object, object is 'alive' as long as when any strong referance point to it, strong referance follow the referance counting techinue it will only deallocate when referance cout will zero
 unowned/referance type/let var: unowned is same as weak. it will not 'own' the object and it will never delloacted it break the retain cycle. it is not work with referance counting. in short unowned means always it has some value.
 copy: when we use copy it means that it create new memory for this and if any changes in copy variable it not reflated on actual variable.
 asign: assign is only work with primitive data type like int,float,double etc
 retain:
 
 
 atomic: default thread safe and slower and run in sequence manner, when we declare a variable as a atomic property its means that only one thread access that variable from multiple thread  and compiler will handle this for not access that variable by multile thread at the same time thats why its thread safe.
 
 nonatomic: not thread safe and faster and run in not sequence manner. When we declare a variable as a nonatomic property its means that multiple thread can be access that variable at the same time due to this it may get unexpected value and it may occure race conditions.
 
 
 readwrite/var: default:  it tell the compiler that the property value will may change or not the value
 readonly/let: it tell the compiler that it will not change the value of property.
 
 */

//*@synthesize creates a getter and a setter for the variable.



// https://varun04tomar.medium.com/open-nuts-and-bolts-of-memory-management-in-ios-swift-part-1-4927b60fccf8
/*
 Static Memory
 *Static allocation happend at compile time.
 *Size of value types are known at compile time, so that compiler knows how much space to reserve.
 * It work with Stack to managing memory
 * There is no Memory reusability
 * not effective
 * Once a memory is allocated can not change the memory size

 
 Dynamic Memory
 *Dynamic allocation happend at run time.
 *Size of ref types are known at run time, so that compiler doesnt knows how much space to reserve.
 * It work with Heap to managing memory
 * There is Memory reusability
 * effective
 * Once a memory is allocated can  change the memory size

 */

/*
 Text
 * It store the machine instruction
 
 Data
 *Swift static variable and constant store in data
 
 
 Stack
 * Value(struct,enum,array,etc) type are store in stack.
 * Even array is used in class(heap) it is also store in stack
 * Method parameter store in stack
 * all the  local variable in function store in stack and remove from it when return the  function
 *faster
 * In multi threaded, each thread has its own stack.
 
 
 Heap
 *Referance(class,closure) type are store in heap
 *Slower(because it search the free space for spocesss and need thread safty)
 *When the process requests a certain amount of memory, the heap will search for a memory address that fulfils this request and return it to the process.
 
 
 **In short: Reference types always goes to the Heap, whereas Value Types always go where they are declared.
 */





// Retain cycle Example
class Fruit{
    
    var personn: Personn?//weak,unowned
    var name: String
    
    
   lazy var fruitsName: (String)-> String = { [unowned self] strValue in //Closure reference cycle
    // if we use any outside the created object inside closue its means that the closure is strongly referance to that object means its will increase the referance count of that object.
    print(strValue)
//    guard let nameValue = self?.name else{
//        return ""
//    }
    return "fruitName: \(self.name)"
    }
    
    

    init() {
        print("init")
        self.name = "apple"
    }
    deinit {
        print("deinit")
    }
}


class Personn{
    var fruit: Fruit? //weak,unowned

    init() {
        print("init")
    }
    deinit {
        print("deinit")
    }
}


class KVO: NSObject{
   @objc dynamic var nameF: String?
}


class ARCMemoryController: UIViewController {
    
  // weak var counterInt: Int! // primitive data type
  // weak var counterValue: String! // primitive data type

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        objCallling()
//        classAndStruct()
        
        /*
        let kvo = KVO()
        kvo.setValue("hp", forKey: "nameF")
        kvo.addObserver(self, forKeyPath: "nameF", options: [.new, .old], context: nil)
        Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { (Timer) in
            kvo.setValue("giri", forKey: "nameF")
        }
        
        Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false) { (Timer) in
            kvo.setValue("home", forKey: "nameF")
        }
        */
        
  
        
    }
    
    

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "nameF"{
            print(change as Any)
        }
    }
    
    
    func objCallling(){
        
        var fruitIns: Fruit? = Fruit() // fruitsRetainCount = 1
//        let fruitNameInstance = fruitIns?.fruitsName("")
//        print(fruitNameInstance as Any)
        
      
        var personnIns: Personn? = Personn() // personRetainCount = 1
        
        fruitIns?.personn = personnIns // personRetainCount = 2
        personnIns?.fruit = fruitIns //fruitsRetainCount = 2
        

                
        fruitIns = nil //personRetainCount = 1
        personnIns = nil //fruitsRetainCount = 1
        
//        Timer.scheduledTimer(withTimeInterval: 10.0, repeats: false) { (Timer) in
//        print(fruitIns)
//        }
                
    }
    
    
    /*
     Class and Struct called
     */
    
    func classAndStruct(){
        let employeeStruct = EmployeeStruct(empId: 101, name: "John", age: 27, department: "IT")
        
        var employeeStructCopy = employeeStruct
        employeeStructCopy.name = "Cavin"
        
        print(employeeStruct.name) //John
        print(employeeStructCopy.name) // Cavin
        
        
        
        let employeeClass = EmployeeClass(empId: 102, name: "Peter")
        employeeClass.age = 25
        employeeClass.department = "Account"
        
        let employeeClassCopy = employeeClass
        employeeClassCopy.name = "Eyan"
        
        print(employeeClass.name)//Eyan
        print(employeeClassCopy.name)//Eyan
        
  }
    
    
    override func didReceiveMemoryWarning(){
        print("didReceiveMemoryWarning")
    }

}

