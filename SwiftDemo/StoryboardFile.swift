//
//  StoryboardFile.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 10/01/22.
//

import Foundation

/*
 //https://medium.com/@linhairui19/difference-between-setneedslayout-layoutifneeded-180a2310e2e6
 //https://aruniphoneapplication.blogspot.com/2017/01/setneedslayout-vs-layoutifneeded.html
 view.layoutIfNeeded() = This method is used to force the view to  immediately  update(position) its layout if any update layout is pending in autolayout.
 view.setNeedsLayout() = This method is used to update the view position it will wait for its turn to update the view.
 view.setNeedsDisplay() = It is used when we want to redraw the view (drawReact)- Using this method it tell the system to view need to redraw. Like we have 2 view for creating line and  when we want to change the line view position at that time we have to set setNeedDisplay other wise it will not update the view line.
//https://stackoverflow.com/questions/14506968/setneedslayout-and-setneedsdisplay
 layoutSubViews() =  It is used when we want to update view layout. We can not directly call this method we first call setNeedLayout or layoutIfNeeded then the layoutSubViews Method call.
 */

/*
 layoutIfNeeded vs setNeedsLayout
 * layoutIfNeeded is used for update immedietly
 * setNeedLayout is used for update the but not immedietly it wait for its cicle
 * layoutIfNeeded used for animated the view and its always sense to use it for purpose of animating while updating view
 * setNeedsLayout not for animation purpose it can not animate the view but just update the view posion
 
 ** Some time  we need to first used setNeedsLayout then use layoutIfNeed becasue setNeedlayout indicate the layoutSubViews for update the view but some time it will implexitly use setNeedsLayout(like when use manually change the storyboard constant using code)
 
 */

/*
 Required 1000
 High 750
 low 250
*/

/*
 Content Hugging:
 * 251 to 251
 * When we increase content hugging it means that it can take only that space which is required for that data
 * content does not want to grow
 
 Content Compression Resistance:
 * 750 to 750
 * When we increase content compression it means that it can take all remaining space  include own data size
 * content does not want to shrink
*/

/*
 UIStoryboardSegue: An object that prepares for and performs the visual transition between two view controllers.
 */

/*
 App upload types
 * Xcode
 * Application Loader
 * Transporter
 */

/*
 Nib
 Xib
 */

/*
 Localization
 https://medium.com/swlh/app-localization-in-swift-ios-swift-guide-baa2c2e4298e
 */

//XCTest
//BlackBox Testing: We can do blackbox testing when we do not know what server developer done in their code or api like checking the api validation on email format.
//UnitTest: When we do test on perticular thing like loginvalidation and api validation saparatly we called it is UnitTest.
//IntegrationTest: When we do test on both loginvalition as well as api valition we called it is intragationTest

/*
Red – Create a test case and make it fail
Green – Make the test case pass by any means.
Refactor – Change the code to remove duplicate/redundancy.
 */


/*
 //https://medium.com/@nishant.kumbhare4/solid-principles-in-swift-73b505d3c63f
 //https://www.youtube.com/watch?v=WJznwyuOHZc
 SOLID
 1.Single Responsibility: Its means that one module has one responsibility just like if you have calling the api at that time we should have:
    * Validation module
    * ServiceHandle Module
    * Response Data Parsing  Module
 
 2.Open Close:
    * Open for extension
    * Close for modification
 
 //https://www.youtube.com/watch?v=VAA6lZsODh4&list=PLb5R4QC2DtFuC7WzUd5bJP3tdVsUcI8E8&index=4
 //https://medium.com/movile-tech/liskov-substitution-principle-96f15559e363
 3.Liskov Substitution: When we use inheritance We can not modify or change the parent class base functionality in child class(Using override). We can achieve this using Protocol.
    *Like we have two variable like heigth and width in super class and doing some calculation and we have inherited it in child class and override these variable and do some thing like update super.hieght using width variable as well ass super.with using height variable this think is break Liskov Subsitution.
 
 //override var height: Int{ super.width = height}
 
 //https://www.youtube.com/watch?v=ZTmSP-ODoZo&list=PLb5R4QC2DtFuC7WzUd5bJP3tdVsUcI8E8&index=5
 4.Interface Segregation: Suppose when we have 5 method in one Protocol  and that protocol is inherited in two class but one class only need 3 method and other class need all 5 method so at that time we forcefully added 2 more method with 3 method so at that time we need to created saparate protocol for these 3method which is common for both class so when we need only 3 method we can inherited one that protocol and when we need 5 method we will inherited both protocols.
 
 5.Dependency Inversion: Class A should not depend on Class B or vise versa and both should be coupled
*/

/*
 
 *CocoaPods is an application level dependency manager for Objective-C, Swift and any other languages that run on the Objective-C runtime.
 *Carthage is a simple dependency manager for macOS and iOS, created by a group of developers from GitHub.
 
 ---------
 Pod File
 Pod init-> it will generate the file in xcode project then open the pod file from xcode and add the dependancy and save the file
 Pod Install-> It will add all dependacy from pod
 
 //brew install cocoapods
 sudo gem install cocoapods(sudo gem install -n /usr/local/bin cocoapods)
 pod setup
 cd (project direct drag link)
 pod init
 open -aXcode podfile # (If it’s already open, add your pod file name. Example: alamofire4.3)
 pod install
 pod update

 Always use XCWorkspace then ever we install pod file
 */

/*
 App Screens
 1 Wire frame
    * static(Normal screens)
    * basic layout
    * structural guidance
    * Wireframes are fast, cheap and easy to create, and quick to be approved.
 2 Prototype
    * advance
    * visual
    * Interaction
    * Look like real app and allow us to ineract with it
 */

/*
 //https://medium.com/wolox/ios-deep-linking-url-scheme-vs-universal-links-50abd3802f97
 //https://medium.com/@MdNiks/custom-url-scheme-deep-link-fa3e701a6295
 //https://www.swiftdevcenter.com/custom-url-scheme-deep-link-ios-13-and-later-swift-5/
 Dynamic linking vs Deep linking vs universal linking vs custom linking
    * URL Scheme- myapp-URL Scheme then put myapp:// on web
    * Universal Linking - apple-app-site-association
 
 apple-app-site-association.json :{
     “applinks”: {
         “apps”: [],
         “details”: [
             {
                 “appID”: “T5TQ36Q2SQ.com.reddit.production”,
                 “paths”: [“*”],
             }
         ]
     }
 }
 */

//sourcetry
/*
 git config
 git init
 git clone
 git add
 git commit
 git diff = check onw commited and repository (existing ) code.(like when we know what previously commited)
 git reset= reset the origin
 git status = check git is available
 git rm = remove branch
 git log = all commited log
 git show
 git tag
 git branch
 git checkout
 git merge= merge the branches
 git remote
 git push
 git pull
 
 imp git stash = it will add own code in local stack when you do not want to commit it currently
 imp rebase = merge the branches
 imp check version(dev,testing, dis)=
 */

/*
 Merge vs Rebase
 
 Merge: in merge when you have feature branch  and you have commit like A commit, B commit, C commit in feature branch when you want to merge the feature branch in main branch then it will look like in main branch.... last commit in main branch Like T commit and new commit feature is U commit. so main history look like ...T->U (it will lost the all feature branch histroy - A commit, B commit, C commit in main branch)
 
 Rebase: in rebase when you have feature branch and you have commit like A commit, B commit, C commit in feature branch when you want to rebase the feature branch in main branch then it will look like in main branch.... last commit in main branch Like T commit and (no need to new commit)so main history look like ...T->A->B->C in linear way
 */



/*
 fetch and merge vs pull
 fetch = when we do fetch it means it will check  first there is any change in branch and any conflit
 pull = It will not check any changes and conflit directly added in our branche then resolve the issue if any.
 merge = after fetch we will  merge the code in branche
 pull = fetch + merge
 */


/*
 Cherry pick=  like we have 2 branch feature1 and feature2 first we commit the on feature1 and we need to work on feature2 but we forget the switch to feature2 branch and worked on feature1 branch and commit it as well how to tranfer second commit to feature2 branch
 or
 when you  want to spacific commit(before push) in other branch then ...
 
 1.checkout (switch to) target branch.
 2.git cherry-pick <commit id>
    Here commit id is activity id of another branch.Eg.
    git cherry-pick 9772dd546a3609b06f84b680340fb84c5463264f
 3.push to target branch
 */



/*
 //https://stackoverflow.com/questions/630453/what-is-the-difference-between-post-and-put-in-http
 Post:- Creating new resource.
 Put:- Replacing existing resource or creating if resource is not exist
 
 PUT    /items/1      #=> update // update spacific iteam from iteam.
 POST   /items        #=> create // Create new items
 
 Social Media/Network Analogy:
 Post on social media: when we post message, it creates new post.
 Put(i.e. edit) for the message we already Posted.
 
 *You can use UPDATE query in PUT whereas you can use create query in POST.
*If you send the same PUT request multiple times, the result will remain the same but if you send the same POST request multiple times, you will receive different results.
  */



