//
//  ClassStructSingleton.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 18/10/21.
//

import Foundation



final class SingletonThreadSafe{
    
    static let share = SingletonThreadSafe()
    
    private init(){}
    
    private var userId: String?
    let barrierQueue = DispatchQueue(label: "com.exm", attributes: .concurrent)
    
    var getUserId: String{
        get{
            return barrierQueue.sync {(userId ?? "")}
        }set{
            barrierQueue.async(flags: .barrier) { //When a writer is writing the data, all other writers or readers will be blocked until the writer is finished writing.
                // No further reads can be made until this block completes. When the block has completed the new value has been set, any getters added after this setter can now run concurrently.
                self.userId = newValue
            }
        }
    }
}



/*
 Singleton is used when we need to access share data in whole app  without  creating new object.
 Singleton used when handle the state managment.
 When we create singleton using class it will create only one object even when we change in any value or property.
 Using final keyword in singleton class, singleton class can not inherit in other class
*/
final class SingletonClass{
    
    static let singletonClass = SingletonClass()
    private init(){
    }
    var state = 10
    
    
    private var userId: String?
    
    let customQueue = DispatchQueue(label: "com.custom", attributes: .concurrent )
    
    var getUserId: String{
        
        get{
            return customQueue.sync{userId ?? ""}

        }set{
            customQueue.async(flags: .barrier) {
                self.userId = newValue
            }
        }
        
    }
    
}

/*
 When we create singleton using struct it will create new object every time when we changes in any value or property so that it wil faild the concept of singleton. In singleton we can only create one object for whole app.
  State managment is not possible using struct singleton.

 */
struct SingletonStruct{
    
    static let singletonStruct = SingletonStruct()
    private init(){
    }
    var state = 50
}




/*
 struct has member wise default initializer. Its value type
 class does not has member wise default initializer if you want class's variable will work without providing default value you should mark variable as a optional. Its referance type.
 */

struct EmployeeStruct {
    var empId: Int
    var name: String
    var age: Int
    var department: String
}

class EmployeeClass {
    var empId: Int
    var name: String
    var age: Int?
    var department: String?
    init(empId: Int, name: String){
        self.empId = empId
        self.name = name
    }
    deinit {
        print("\(#function) Employee: \(name)")
//        print("\(type(of: self))")
    }
}



/*
 Class and Struct called
 */

func classAndStruct(){
    
    let employeeStruct = EmployeeStruct(empId: 101, name: "John", age: 27, department: "IT")
    
    var employeeStructCopy = employeeStruct
    employeeStructCopy.name = "Cavin"
    
    print(employeeStruct.name) //John
    print(employeeStructCopy.name) // Cavin
    
    
    let employeeClass = EmployeeClass(empId: 102, name: "Peter")
    employeeClass.age = 25
    employeeClass.department = "Account"
    
    let employeeClassCopy = employeeClass
    employeeClassCopy.name = "Eyan"
    
    print(employeeClass.name)//Eyan
    print(employeeClassCopy.name)//Eyan
}



class A{
    var g = 10
    func all(){
        print("A\(g)")
    }
}

class B: A{
    override func all(){
        print("B: \(g+10)")
    }
}



class C: B{
    override func all(){
        print("C\(g)")
    }
}



























