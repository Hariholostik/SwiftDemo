//
//  ReactNative.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 7/03/22.
//

import Foundation

//npx react-native init AwesomeProject


//"react": "17.0.1",
//"react-native": "0.64.1",
//"react-redux": "^7.2.4",
//"redux": "^4.1.1"

/*
 Class Component:
 
 import React, {Component} from 'react';
 import {View,Text} from 'react-native'
 
 class Demo extends React.Component {
 
   constructor(props){
      super(props);
         this.state={
         counter: 0
         };
    }
     arrowAction=()=>{
          this.setState({counter: 1});
     }
 
     render(){
          retrun(
          <Text/>
          )
     }
 }
 
 
 
 export default Demo
 Functional Component: Its is a simple java script function. It is a state less.
 
 
 
 import React,{useState, useEffect} from 'react'
 import {View, Text} from 'react-native'
 
 const demo =(props)=>{
 const [count, setCount] = useState(0);
 const [name, setName] = userState("");
 
 useEffect(() =>{
 // it will only called when ever setCount will change(because it contian in array) if setName will change it will not called this function(because it doesnt contian in array)
 },[count]); // array [] contain these value which we want to called useEffect when any change in happend the that array contain value.
 
     action(){
       setCount(1)
       setName("hg")
     }
 
     return(
      <Text>count</Text>
    )
 }
 
 
 Hooks:
 useState: It is used to manage the state in functional component
 useEffact: It is called when ever any changes in functional coponent.
 
 Redux:
 
 //https://www.imaginarycloud.com/blog/react-native-redux/#:~:text=Redux%20is%20a%20javascript%20library,modified%20through%20actions%20and%20reducers.
 *Redux is a javascript library made to help you manage the state of your application.
 
 action: all action available here(insert, update)
 reducer: get the action type and combine the data and pass to store
 store: store and update the data
 
 //fatching data from store
 const mapStateToProps = (state) =>{
   return{countValue:state.allProducts.products}
 }
 
 //update the store
 //ActionTypes.COUNTER_CHANGE
 const mapDispatchToProps = (dispatch) =>{
 return{
   // changeCount: (count) =>{dispatch({type: ActionTypes.COUNTER_CHANGE , payload: count})}
   setProducts: (products) =>{dispatch({type: ActionTypes.SET_PRODUCTS , payload: products})}
 }
 
 
 Arrow function:

 */

/*
 Project Config Files:-
 
 1.app.json: - it contain name of project and app dispaly name
 2.package-lock.json: It is used to lock the specific version of module because when you take project from source control if that project contain lock file it means when we do npm i it will only install that specific version other wise it will install latest version of that module
 3.package.json: Its  contain like module name version script(command like run ios and android),properties description.
 */

/*
 *Node Package Manager
 * npm is the package manager for the Node JavaScript platform.
 npm vs yarn
 */
