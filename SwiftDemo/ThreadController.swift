//
//  ThreadController.swift
//  SwiftDemo
//
//  Created by Hari Giri (IOS) on 14/09/21.
//
//https://mobikul.com/blog/
//https://www.appcoda.com/grand-central-dispatch/
//https://developer.apple.com/library/archive/documentation/General/Conceptual/ConcurrencyProgrammingGuide/OperationQueues/OperationQueues.html

//Instrument
//UnitTesting

/*
 LLVM-based tools:-
 https://www.avanderlee.com/swift/thread-sanitizer-data-races/
*Thread Sanitizer/TSan tool- For identify the data race in ios we use :- go to edit schema-> diagnostics-> selcted thread senitizer-> run again
 
*Address Senitizer/ASan tool- For identify the memory corrupt issue in ios we use.
*Main Thread Checker- This tool varify that system api must run on main tread(UI must create and update on main thread).
  */


//GCD: Grand Central Dispatch (GCD) is a framework provided by Apple
//1.Serial Queue In GCD(Grand central Dispatch)
//2.Cuncurrent Queue In GCD(Grand central Dispatch)

/*
 Sync:- blocking the calling thread until task is finished.
 Async:- directly return from calling thread without blocking.
 
 * Sync and Async method flow FIFO technique- Sync method always intialize as FIFO manner and finish as FIFO manner but in Async method always initalize as FIFO manner and it will not finished as FIFO manner.
 */
/*
Serial Queue In GCD(Grand central Dispatch)

 * if we have one instance queue for multiple async method task will execute in serial manner(print data one by one).
 * if we have one instance queue for multiple sync method task will execute in serial manner.
 * if we have multiple instance queue for multiple async method task will execute in parallal or simultaneously manner. example if we have one instance queue have two async method and other instance queue have two async method so first instance queue of two method execute like serial manner same for second instnace queue(but both instance queue execute like parallal manner)
 Example:-
 First instance have F1Asynce Method F2Asynce method
 F1Asynce print 0 to 5 or F2Asynce print 6 to 10
 
 Second instance have F3Asynce Method F4Asynce method
 F3Asynce print 11 to 15 or F4Asynce print 16 to 20
 
 data mybe print like: 11,0,1,2,3,4,12,13,14,15,5,6,7,8,16,17,18,19,9,10,20
 
 case1: both instance execute like parallal manner.
 case2:  first instance of two asynce method execute like serial manner it  will always print like 0,1,2...8,9,10 manner same for second instance
 * if we have one instance queue for multiple sync and async method task will execute in serial manner.
 * if we have multiple instance queue for multiple  sync and async method the task will execute in parallal manner
*/

/*
 Cuncurrent Queue In GCD(Grand central Dispatch)
 * if we have one instance queue for multiple async method the task will execute in parallal manner.// same for multiple instance
 * if we have one instance queue for multiple sync and async method the task will execute in parallal manner.// same for multiple instance
 * if we have one instance queue for multiple sync method the task will execute in serial manner.// same for multiple instance
 */


/*
 Data Race:- its occur when same memory access by two thread at same time like value from array access in main thread same as in background thread  enter the value in same array at same time.
 Data races occur when the same memory is accessed from multiple threads without synchronization.
 =issues:
 Unpredictable behavior
 Memory corruption
 Flaky tests
 Weird crashes
 
 You might have a crash on startup that is not occurring again the second time you start your app. If this is the case, you might be dealing with a Data Race.
 
 
 DispatchWorkItem:-
 When we dnt want to use code block then we use DispatchWorkItem. When the work is done in DispatchWorkItem then the inbuild method called notify()(on main thread) after that workitem execution  on  called inbuild method perform()
 
 *Main Queue:- Serial Queue
 *Global Queue:- Concurrent Queue
 *Custom Queue:- Bydefault custom ia queue serial queue, you can make it even concurrent queue.
 */

/*
 Dispatch Group:- A group of task that we will monitor as single unit.
Method: Enter(): It will indicate that the the task is executing , Leave()= it will indicate that the task is completed and notify() called after the task is finished.
 */
/*
 //https://www.youtube.com/watch?v=6rJN_ECd1XM&t=2s
 Dispatch Semaphore:- We can execute task one by one. Semaphore resolve the race condition. We can set how many no. of thread can access any share resource in semaphore.
 Method:  Wait(): It will tell the semaphore the task is occupied with other thread.,Signal()= It will tell the semaphore the the task is not occupied by any thread.,
 */


/*
 NSOperationQueue:- Operations Queues are Cocoa’s high-level abstraction on GCD. Operation Queue will execute number of  operation  based on
 thier priorty. It can execute task concurrently or serial manner. Operation queue has ability to cancel all operation at a time and it can fix how many number of operation will execute and it can add dependancy among operation or queue.
 
 Operation/BlockOperation/NSInvocationOperation/CustomOperation: Operation is a abstract class and it represent the data associated with single unit. We can paused, resumed, and cancelled the operation.
 BlockOperation: Its a concrete subclass of operation it can manage  concurrant execution of one or more block.
 
 waitUntilFinished: When we set true then it will hold the current thread with in operations queue till finished the operations or if we set false it will not hold the current thread with in operation queue..
 */


/*
GCD vs NSOperationQueue(NSOperationQueue created over the GCD)
 GCD-
 * GCD is C based low level api
 * Easly to Implement
 * Light Weight- lock-free
 
 Operation Queue
 * Control on Operation- Pause, Cancel, Resume
 * Dependency- Add dependacny between Operation
 * State Of Operation- Ready, Executing, Finished
 * Max Number of Operation- We can set how many no of operation can run cuncurrently.
 * with NSOperation, KVO can be possible(know the state of operation).
 * NSOperationQueue is complex and heavy-weight- lots of locking
*/



import UIKit
//Why we use self in async block when we update UI
// Why we are not allow to update on main thread sync method
class ThreadController: UIViewController {

    private var name: String = "india "
    private lazy var lazyName: String = "accessing lazy variable"
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        DispatchQueue.main.async {
//        }
//        DispatchQueue.main.sync {
//        }
//        DispatchQueue.global().async {//background
//        }
//        DispatchQueue.global().sync {//background
//         }
        
//        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
//            print("ThreadController")
//        }
//
//        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
//            print("ThreadController1")
//        }
   }
    
    
    override func viewDidAppear(_ animated: Bool){
        super.viewDidAppear(animated)
//        customQueueF()
//         dataRace()
//        globalQOSQueueF()
//        dispatchGroupInit()
//        performUsingSemaphore()
        performNSOperationQueue()

    }
    
    
    
    func dataRace(){
        
        DispatchQueue.global().async{
            self.name.append("holostik")
        }
        
        
//        DispatchQueue.main.async {
            print(self.name)

//        }
        
        DispatchQueue.global().async {
            print(self.lazyName)
        }
        
//        DispatchQueue.global.async {

        print(self.lazyName)
            
//        }
        
    }
    
    
    
    func customQueueF(){
        

      let customQueue = DispatchQueue(label: "com.custom.queue")
        
//        let customQueue = DispatchQueue(label: "com.custom.queue", qos: .userInitiated, attributes: .concurrent)

            customQueue.sync{// background thread

            for item  in 0..<10{
                print(item,Date())
//                sleep(1)
            }
//
//            for item2  in 10..<20{
//                print(item2,Date())
//            }
        }
        
      /*  customQueue.sync{// background thread

            for item  in 20..<30{
                print(item)
            }
//            for item2  in 30..<40{
//                print(item2)
//            }
        }
        */
       
        
        
       
        let customQueue2 = DispatchQueue(label: "com.custom.queue2")
//        let customQueue2 = DispatchQueue(label: "com.custom.queue2", qos: .userInitiated, attributes: .concurrent)

        customQueue2.async{// background thread

//            for item  in 40..<50{
//                print(item,Date())
//            }

            for item2  in 50..<60{
                print(item2)
            }
        }
//
//        customQueue2.async {// background thread
//
//            for item  in 60..<70{
//                print(item,Date())
//            }
//            for item2  in 70..<80{
//                print(item2)
//            }
//        }
     
        
        
//        DispatchQueue.main.async {
//
//            for item  in 0..<10{
//                print(item,Date())
//            }
//        }

            
//        DispatchQueue.main.async {

//            for item  in 20..<30{
//                print(item)
////                sleep(1)
//
//            }
//        }



//        for item  in 200000..<500000{
//            print(item)
//        }
            
}
    
    
    // Background Queue
    func globalQOSQueueF(){

        let globalSerialQueue = DispatchQueue.global()
                
        globalSerialQueue.async{
            for i in 0..<10{
                print(i)
            }
        }
        
        globalSerialQueue.sync{
            for i in 10..<20{
                print(i)
            }
        }
                 
    }
    
    
    //============================================================
    /*
     Dispatch Group
     */
    
    func dispatchGroupInit(){
         
        
        let dispatchGlobal = DispatchQueue.global()
        
        let dispatchGroup = DispatchGroup()
        
//        dispatchGroup.wait(timeout: T##DispatchTime)
//        dispatchGroup.wait()
        
//        getUserProfile { (isSuccess) in
//            print("getUserProfile")
//            dispatchGroup.leave()
//        }
  
            dispatchGlobal.async {
                dispatchGroup.enter()
        self.serviceHandler(type: "first", urlS: "https://www.learningcontainer.com/wp-content/uploads/2020/07/sample-jpg-file-for-testing.jpg") { (isSuccess, type) in
                      print(type)
                dispatchGroup.leave()
                }
            }
        
//        dispatchGlobal.async {
                dispatchGroup.enter()
        self.serviceHandler(type: "second", urlS: "https://www.learningcontainer.com/wp-content/uploads/2020/07/Sample-JPEG-Image-File-Download-scaled.jpg") { (isSuccess, type) in
                    print(type)
                    dispatchGroup.leave()
                }
//        }

//        dispatchGlobal.async {
                dispatchGroup.enter()
        self.serviceHandler(type: "third", urlS: "https://www.learningcontainer.com/wp-content/uploads/2020/07/Sample-JPEG-Image-File-Download.jpg") { (isSuccess, type) in
                    print(type)
                    dispatchGroup.leave()
                }
//        }
        
//        _ = dispatchGroup.wait(timeout: .distantFuture)
        
        

//        for i in 0..<5{
//            print(i)
//        }

        dispatchGroup.notify(queue: .main) {
            print("All task has been completed")
        }
    }
    
    
    
    
    func getUserProfile(handler:(_ isSuccess: Bool)->Void){
        
            for i in 0..<10{
                print(i)
        }
    
        handler(true)
    }
    func getTotalEarnedPoints(handler:(Bool)->Void){
        for i in 10..<20{
            print(i)
        }
        handler(true)
    }
    
    func getScanHistory(handler:(Bool)->Void){
        for i in 20..<30{
            print(i)
        }
        handler(false)
    }
    
    
    // @escaping
    func serviceHandler(type: String, urlS: String, handler:@escaping(Bool,String)->Void){
 
        let urlString = urlS.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        guard let url = NSURL(string: urlString) as URL? else {
            return
        }

        URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                let img = UIImage.init(data: data!)
                print(img as Any)
                print(urlS)
                handler(true, type)
            }
        }.resume()
        
//        handler(true, type)

    }
    
    
    //============================================================
    /*
     Dispatch Semaphore
     */
    func performUsingSemaphore() {
        
        let dispatchglobal = DispatchQueue.global()
        let semaphore = DispatchSemaphore(value: 1)
        
        for i in 0..<2{
            
        dispatchglobal.async {
            
              let songNumber = i + 1
              semaphore.wait()
              print("Downloading song", songNumber)
              sleep(2) // Download take ~2 sec each
              print("Downloaded song", songNumber)
              semaphore.signal()
           }
      }
        
        
        return
        
        var shareResource = ""
        
        print("1=================")

        dispatchglobal.async {
            semaphore.wait()
            print("1st Semaphore enter")
            sleep(2)
            print("1st block completed")
            shareResource += "time"
            semaphore.signal()
        }
    
        
        print("2=================")
        dispatchglobal.async {
            semaphore.wait()
            print("2nd Semaphore enter")
            sleep(1)
            print("2nd block completed")
            shareResource += " school"

            semaphore.signal()
            
            print(shareResource)
            
        }
    }
    
    
    //===========================================================
    
    /*
    NSOperationQueue
    */
    func performNSOperationQueue(){
    //https://ali-akhtar.medium.com/concurrency-in-swift-operations-and-operation-queue-part-3-a108fbe27d61
        
        /*
        let operation = BlockOperation{
            for i in 1...10{
                print(i)
                print(Unmanaged.passUnretained(Thread.current).toOpaque())
                print(Thread.isMainThread)
                Thread.sleep(forTimeInterval: 1)
            }
        }
         
        operation.qualityOfService = .background
        operation.queuePriority = .normal
        operation.cancel()
        operation.start()
        print("start finshed")
        print("cancel finshed")
        */
      
    
        
       /*
        let operation = BlockOperation()
        
        operation.completionBlock={
            print("completion block \(Thread.current)") //work like dispatch group notify , called when all task will complete.
            print(Thread.isMainThread)
        }
        operation.addExecutionBlock {
            print(Thread.current)
            print(Thread.isMainThread)
            for i in 1...10{
                print(i)
            }
        }
        
        operation.addExecutionBlock {
            print(Thread.current)
            print(Thread.isMainThread)
            for j in 11...20{
                print(j)
            }
        }
        
        operation.start()

//        DispatchQueue.global(qos: .background).async {
//            print("running on \(Thread.current)")
//            operation.start()
//        }
        print("block operation has been finished")
        */
        
      
        
/*
    let operation = BlockOperation()
        operation.addExecutionBlock{
            for i in 1...5{
                print(i)
            }
        }
   
        operation.addExecutionBlock{
            for j in 6...10{
                print(j)
            }
        }
        
        let current = Thread {
            operation.start()
        }
        
        current.start()
        
        */
        
   //When  we use operation queue then we do not need to manually start operation.
        
        
        let operationQueue = OperationQueue()
        
        let operation1 = BlockOperation{
            for i in 1...5{
                print(i)
            }
        }
        
        let operation2 = BlockOperation{
            for j in 6...10{
                print(j)
            }
        }
        
        let operation3 = BlockOperation{
            for j in 11...15{
                print(j)
            }
        }
        
        operationQueue.maxConcurrentOperationCount = 3 //-1= default. it will decive how many operation will execute at same time. if u will set 0 then it will not execute any operation.
//        operation3.addDependency(operation1)
//        operation3.addDependency(operation2)
//        operation2.addDependency(operation1)

//        operation3.addDependency(operation1)
//        operation3.addDependency(operation2)
        
        
//        operationQueue.addOperation(operation1)
//        operationQueue.addOperation(operation2)
//        operationQueue.addOperation(operation3)
        
        operationQueue.addOperations([operation1,operation2,operation3], waitUntilFinished: true)
        print("last line")
        
      
    }
    
}

//728
//Oct: 54384
//Sep: 54741
//Aug: 55112
